//firebase deploy --only functions
const functions = require('firebase-functions');
const axios = require('axios');
const mapUrls = functions.config().dart_dbl;
const urls = ['https://3353cc48d0bd.ngrok.io', 'https://0e833fb08a2f.ngrok.io', 'https://cd1b44f47033.ngrok.io'];
  
function sendRequest(data) {
    const mapUrls = functions.config().dart_db;
    const futures = [];
    if (mapUrls) {
        for (let _url in mapUrls) {
            futures.push(axios.post(urls[_url], data));
        }
    }
    if (urls) {
        if (typeof urls === "string") {
            futures.push(axios.post(urls, data));
        } else {
            for (let _url in urls) {
                futures.push(axios.post(urls[_url], data));
            }
        }
    }

    return Promise.all(futures);
}
  
exports.newChatCreation = functions.database.ref('chat_topic/user_to_user/{key1}/cid')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/user_to_user/{key1}/cid'
        };

        return sendRequest(data);
    });
exports.deleteUselessImage = functions.database.ref('images_container/{key1}')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'images_container/{key1}'
        };

        return sendRequest(data);
    });
exports.changeRating = functions.database.ref('user_data/{key1}/sum_rating')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'user_data/{key1}/sum_rating'
        };

        return sendRequest(data);
    });
exports.creatingUser = functions.database.ref('user_data/{key1}/user_phone')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'user_data/{key1}/user_phone'
        };

        return sendRequest(data);
    });
exports.onMessage = functions.database.ref('chat_topic/user_to_user/{key1}/messages/{key2}')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/user_to_user/{key1}/messages/{key2}'
        };

        return sendRequest(data);
    });
exports.onAccountRemoved = functions.database.ref('user_data/{key1}/user_information/is_removed')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'user_data/{key1}/user_information/is_removed'
        };

        return sendRequest(data);
    });
exports.changeHouse = functions.database.ref('user_data/{key1}/houses/{key2}')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'user_data/{key1}/houses/{key2}'
        };

        return sendRequest(data);
    });
exports.creatingChat = functions.database.ref('chat_topic/offer2room/{key1}/conversation_users')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/conversation_users'
        };

        return sendRequest(data);
    });
exports.newMessage = functions.database.ref('chat_topic/user_to_user/{key1}/messages/{key2}/uid')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/user_to_user/{key1}/messages/{key2}/uid'
        };

        return sendRequest(data);
    });
exports.newMessageTimestamp = functions.database.ref('chat_topic/user_to_user/{key1}/messages/{key2}/timestamp')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/user_to_user/{key1}/messages/{key2}/timestamp'
        };

        return sendRequest(data);
    });
exports.changeRequestInfo = functions.database.ref('user_data/{key1}/requests/{key2}/request_information')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'user_data/{key1}/requests/{key2}/request_information'
        };

        return sendRequest(data);
    });
exports.offerObjectCreation = functions.database.ref('chat_topic/offer2room/{key1}/conversation_users/owner_user')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/conversation_users/owner_user'
        };

        return sendRequest(data);
    });
exports.statusesHandler = functions.database.ref('chat_topic/offer2room/{key1}/conversation_users/users_statuses')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/conversation_users/users_statuses'
        };

        return sendRequest(data);
    });
exports.ownerRatingAndReview = functions.database.ref('chat_topic/offer2room/{key1}/review/owner_review/rating_and_review')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/review/owner_review/rating_and_review'
        };

        return sendRequest(data);
    });
exports.requestorRatingAndReview = functions.database.ref('chat_topic/offer2room/{key1}/review/request_user_review/rating_and_review')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/review/request_user_review/rating_and_review'
        };

        return sendRequest(data);
    });
exports.ownerReview = functions.database.ref('chat_topic/offer2room/{key1}/review/owner_review')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/review/owner_review'
        };

        return sendRequest(data);
    });
exports.requestorReview = functions.database.ref('chat_topic/offer2room/{key1}/review/request_user_review')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/review/request_user_review'
        };

        return sendRequest(data);
    });
exports.requestorReviewAnswer = functions.database.ref('chat_topic/offer2room/{key1}/review/request_user_review/review_answer')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/review/request_user_review/review_answer'
        };

        return sendRequest(data);
    });
exports.ownerReviewAnswer = functions.database.ref('chat_topic/offer2room/{key1}/review/owner_review/review_answer')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/review/owner_review/review_answer'
        };

        return sendRequest(data);
    });
exports.changeRequest = functions.database.ref('user_data/{key1}/requests/{key2}/request_information/last_change')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'user_data/{key1}/requests/{key2}/request_information/last_change'
        };

        return sendRequest(data);
    });
exports.copyAddress = functions.database.ref('user_data/{key1}/houses/{key2}/house_information/address')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'user_data/{key1}/houses/{key2}/house_information/address'
        };

        return sendRequest(data);
    });
exports.requestorStatusTracker = functions.database.ref('chat_topic/offer2room/{key1}/conversation_users/users_statuses/request_user_status')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: 'chat_topic/offer2room/{key1}/conversation_users/users_statuses/request_user_status'
        };

        return sendRequest(data);
    });