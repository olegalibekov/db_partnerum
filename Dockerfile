# Use Google's official Dart image.
# https://hub.docker.com/r/google/dart-runtime/
FROM google/dart-runtime

# Service must listen to $PORT environment variable.
# This default value facilitates local development.
ENV PORT 8080

# The target variable
ENV TARGET "World + Dog"

#ENV ALGOLIA_APP_ID 5COMX2KEPF

#ENV ALGOLIA_API_KEY fa5d442ce41f375cc30ff20d9ed5ee03

#ENV ALGOLIA_INDEX_NAME prod_HOUSES

