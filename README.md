# db_partnerum

A partnerum project db package.

## Getting Started

## Building and deploying the sample

Once you have recreated the sample code files (or used the existing files) 
you're ready to build and deploy the sample app.

1. Build your container image using Cloud Build, by running the following command from 
   the top level directory containing the Dockerfile:
   
   ```gcloud builds submit --tag gcr.io/golden-memory-243607/db_serve ```
   
   where [partnerum] is your GCP project ID. You can get it by running gcloud config get-value project.
   
   Upon success, you will see a SUCCESS message containing the image name (gcr.io/[partnerum]/db_serve). The image is stored in Container Registry and can be re-used if desired.

2. Deploy the build above using the following command:

   ```gcloud beta run deploy --image gcr.io/golden-memory-243607/db_serve```
   
    where [partnerum] is again your GCP project ID. You can get it by running gcloud 
    config get-value project.

    When prompted, select a region (for example us-central1), confirm the service name, 
    and respond 'y' to allow unauthenticated invocations.

    Then wait a few moments until the deployment is complete. On success, the 
    command line displays the service URL. Visit your deployed container by opening the 
    service URL in a web browser.
    
    Congratulations! You have just deployed a Dart application packaged in a container 
    image to Cloud Run. Cloud Run automatically and horizontally scales your 
    container image to handle the received requests, then scales down when demand 
    decreases. You only pay for the CPU, memory, and networking consumed during 
    request handling. If you change your project you can just re-build and redeploy 
    as you wish.


## Removing the sample app deployment

To remove the built image you can [delete](https://cloud.google.com/container-registry/docs/managing#deleting_images) the image.
You can also delete your GCP project to avoid incurring charges. Deleting your GCP project stops billing for all the resources used within that project.


[![Run on Google Cloud](https://storage.googleapis.com/cloudrun/button.svg)](https://console.cloud.google.com/cloudshell/editor?shellonly=true&cloudshell_image=gcr.io/cloudrun/button&cloudshell_git_repo=https://StarCabbage:07c8bd8aa8e79a620b6cfd5c227ff5923f243609@github.com/StarCabbage/db_partnerum.git)