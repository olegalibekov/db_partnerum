
import 'package:db_partnerum/src/manager/manager.dart';import '../src/../db_partnerum.dart';
import 'package:event_container/event_container.dart';

// import 'manager/database_manager.dart';
// import 'models/root_model.dart';

typedef RealtimeIdTokenGenerator = Future<String> Function();
typedef DbSaver = Future<bool> Function(String key, String value);
typedef DbGetter = Future<String> Function(String key);

class Path {
  static final Path _singleton = Path._internal();

  Root get root => Root('');
  var projectId = 'golden-memory-243607';
  var baseRoot;
  var storageRoot;
  var localhost;

  close() {
    databaseManager.operationsFlow
        .add(EventContainer<DbEvent, dynamic>(DbEvent.close, null));
  }

  var isWeb;

  ///Сюда надо записать локальный ключ.
  var localKey = null;
  var streamCacheOn = false;
  var etagCacheOn = false;
  var asyncMode = false;
  var optimizationMode = false;
  RealtimeIdTokenGenerator realtimeIdTokenGenerator,
      realtimeIdTokenGeneratorServer;

  ///Для sharedPrefs
  DbSaver saver;
  DbGetter getter;

  init(String uid) async {
    localKey = uid;
    await databaseManager.localDatabaseManager.loadFromLocal();
  }

  String rootPath;

  factory Path() {
    return _singleton;
  }

  Path._internal();
}

final db = Path();
final root = db.root;
final databaseManager = DatabaseManager();

///Так делать нельзя!
// var myUserStream = myUser?.stream;
// myUserStream.listen((event) { });
// myUserStream.listen((event) { });
///Надо так:
// myUser?.stream?.listen((event) { });
// myUser?.stream?.listen((event) { });
