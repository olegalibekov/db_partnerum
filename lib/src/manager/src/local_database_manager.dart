import '../../../db_partnerum.dart';
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'package:xxtea/xxtea.dart';

extension ReplaceFunction on Map {
  replaceKey(from, to) {
    var obj = remove(from);
    this[to] = obj;
  }
}

enum OperationType {
  //Это мержит данные
  merge,
  //это заменяет на новые
  update,
  remove
}

///интеграция с умным поиском
class LocalDatabaseManager {
  Map<DbBaseType, dynamic> ramData = {};
  var saver = Saver();

  // dynamic operator [](List path) {
  //   return ramData[DbUtils.stringifyPath(path).join('/')];
  // }
  //
  // void operator []=(List path, dynamic value) {
  //   ramData[DbUtils.stringifyPath(path).join('/')] = value;
  // }

  dynamic operator [](DbBaseType object) {
    var path = object.dbPath;
    var map;
    if (path.first is Root) {
      map = rootMaps[path.first];
    } else {
      map = tmpValues[path.first];
    }
    var value;
    var tmpPath = path.toList()..removeAt(0);
    for (var i = 0; i < tmpPath.length; i++) {
      var tmp = map[tmpPath[i].key];
      if (tmp == null) return null;
      if (tmp is Map)
        map = tmp;
      else
        value = tmp;
    }

    return value ?? map;
  }

  isContainsObject(DbBaseType baseType) {
    var _tmpObj = this[baseType];
    return _tmpObj != null;
  }

  write(DbBaseType object, dynamic value,
      {OperationType type = OperationType.update, String eTag}) async {
    if (value is Map && eTag != null) {
      value[RealtimeDatabaseManager.Etag] = eTag;
    }
    var path = object.dbPath;

    var map = {};
    if (path.first is Root) {
      map = rootMaps[path.first];
    } else {
      map = tmpValues[path.first];
    }
    var tmpMap = map;
    var tmpPath = path.toList()..removeAt(0);
    for (var i = 0; i < tmpPath.length; i++) {
      if (tmpPath.length - 1 != i) {
        if (tmpMap[tmpPath[i].key] == null) tmpMap[tmpPath[i].key] = {};
        tmpMap = tmpMap[tmpPath[i].key];
      }
    }
    switch (type) {
      case OperationType.merge:
        tmpMap[tmpPath.last.key] = value;
        break;
      case OperationType.update:
        tmpMap[tmpPath.last.key] = value;
        break;
      case OperationType.remove:
        if (tmpMap.containsKey(tmpPath.last.key)) {
          tmpMap.remove(tmpPath.last.key);
        }
        break;
    }

    if (db.localKey != null) {
      saver.saveToLocal(path.first);
    }
  }

  loadFromLocal() async {
    var key = db.localKey.hashCode.toString();
    var string = await db.getter(key);
    if (string == null || string.isEmpty) return;

    final decrypted = xxtea.decryptToString(string, db.localKey);

    if (decrypted != null && decrypted.isNotEmpty) {
      var value = jsonDecode(decrypted.toString());
      if (value is Map) {
        rootMaps[root] = value;
      }
    }
  }

  _changeFrom2_v1(Map map, String from, String to) {
    return map.map((key, value) {
      String _key = key;
      if (_key == from) {
        _key = to;
      }
      if (value == from) {
        return MapEntry(_key, to);
      } else if (value is Map) {
        return MapEntry(_key, _changeFrom2_v1(value, from, to));
      }
      return MapEntry(_key, value);
    });
  }

  _changeFrom2(Map map, String from, String to) {
    for (var key in map.keys.toList()) {
      var value = map[key];
      if (value == from) {
        map[key] = to;
      } else if (value is Map) {
        map[key] = _changeFrom2(value, from, to);
      }

      if (key == from) map.replaceKey(from, to);
    }
    return map;
  }

  deepMerge(Map main, Map second) {
    for (var key in main.keys.toList()) {
      if (second.containsKey(key)) {
        main[key] = second[key];
      } else {
        main.remove(key);
      }
    }
  }

  push2TruePath(DbBaseType object, String pushId, String truePath) {
    return _changeFrom2(rootMaps[object.dbPath.first], pushId, truePath);
  }

  syncIt(DbBaseType object) {
    var value = tmpValues.remove(object);
    write(object, value, type: OperationType.update);
  }

  static path2map(String rawPath, value) {
    var path = rawPath.split('/');
    insider(List path, value) {
      if (path.length == 1)
        return {path.first: value};
      else
        return {path.removeAt(0): insider(path, value)};
    }

    if (path[1].isEmpty) {
      return value;
    } else {
      path.removeAt(0);
      return insider(path, value);
    }
  }

  static Map<Root, Map> rootMaps = {};
  static Map<Root, dynamic> anonimMaps = {};

  // static put(DbBaseType key, DbBaseType value) {
  //   if (!tunnel.containsKey(key)) {
  //     tunnel[key] = Set<DbBaseType>();
  //   }
  //   tunnel[key].add(value);
  // }

  static wayThrough(DbBaseType key, value) {
    // if(map){
    //
    // }
  }

  static Map<DbBaseType, dynamic> tmpValues = {};
}
