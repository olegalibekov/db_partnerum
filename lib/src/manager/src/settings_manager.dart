
import '../../../db_partnerum.dart';
import 'package:googleapis/calendar/v3.dart';

enum SettingsState { on, off, unsetted }

class SettingsPair {
  SettingsState _currentValue;

  SettingsState get currentValue => _currentValue ?? defaultValue;

  set currentValue(SettingsState state) => _currentValue = state;
  final SettingsState defaultValue;

  SettingsPair(this.defaultValue,
      [this._currentValue = SettingsState.unsetted]);

  bool isOn() {
    return currentValue == SettingsState.on;
  }

  bool isOff() {
    return currentValue == SettingsState.off;
  }

  on() {
    _currentValue = SettingsState.on;
  }

  off() {
    _currentValue = SettingsState.off;
  }

  @override
  String toString() {
    return '$SettingsPair($currentValue, default: $defaultValue)';
  }
}

class DbBaseTypeSettings {
  ///[packet] позволяет загружать обьекты не постепенно, а разом.
  var packet = SettingsPair(SettingsState.off);

  ///Блокирует выполнение любых операций базы данных, синхронизация останавлена
  var lock = SettingsPair(SettingsState.off);

  ///Прощай порядок - в этот момент все запросы могут выполниться единовременно, правда,
  /// если они находятся в разных путях.
  /// Иначе, сначала обязательно происходит полная оптимизация.
  /// При работе с отдельным корнем асинхронность полная, всегда.
  var async = SettingsPair(SettingsState.off);

  ///Работает следующим образом. База отслеживает удачный момент(например загрузка первого запроса),
  ///чтобы накопить данные
  ///и сделать результирующий этап.
  var optimization = SettingsPair(SettingsState.off);

  ///Позволяет за один ход загрузить все изменения по upload
  var oneTimeUploadPacket = SettingsPair(SettingsState.off);

  ///Важно сделать так, чтобы первый параметр был старше.
  static _settingsStateWinner(SettingsState first, SettingsState second) {
    if (second != SettingsState.unsetted) {
      return second;
    }
    if (first != SettingsState.unsetted) {
      return first;
    }
    return SettingsState.unsetted;
  }

  static DbBaseTypeSettings completeSettings(DbBaseType baseType) {
    var completeSettings = DbBaseTypeSettings();
    baseType.dbPath.forEach((element) {
      completeSettings.packet.currentValue = _settingsStateWinner(
          completeSettings.packet.currentValue,
          element.settings.packet.currentValue);
      completeSettings.async.currentValue = _settingsStateWinner(
          completeSettings.async.currentValue,
          element.settings.async.currentValue);
      completeSettings.lock.currentValue = _settingsStateWinner(
          completeSettings.lock.currentValue,
          element.settings.lock.currentValue);
      completeSettings.optimization.currentValue = _settingsStateWinner(
          completeSettings.optimization.currentValue,
          element.settings.optimization.currentValue);
      completeSettings.oneTimeUploadPacket.currentValue = _settingsStateWinner(
          completeSettings.oneTimeUploadPacket.currentValue,
          element.settings.oneTimeUploadPacket.currentValue);
    });
    return completeSettings;
  }
}
