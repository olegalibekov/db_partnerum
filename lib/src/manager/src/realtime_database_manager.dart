import 'package:db_partnerum/src/manager/manager.dart';
import '../../../db_partnerum.dart';

import 'dart:convert';

import 'package:eventsource/eventsource.dart';
import 'package:http/http.dart' as http;

class ResponseContainer {
  final int code;
  final String message;
  final value;
  final String etag;
  final DbEvent event;

  ResponseContainer(this.code, this.message, this.value, this.etag, this.event);
}

class RealtimeDatabaseManager {
  static Map<String, Future<http.Response>> tmpKeysQueueMap = {};
  static const ETAG_HEADER = {'X-Firebase-ETag': 'true'};
  static const Etag = 'etag';
  static const EtagIfMatch = 'if-match';

  static get host {
    return 'https://${db.baseRoot ?? db.projectId}.firebaseio.com';
  }

  static Future<String> _tokenPart() async {
    var authParameterName =
    db.realtimeIdTokenGeneratorServer == null ? 'auth' : 'access_token';
    return '?${authParameterName}=${await DbUtils.token()}';
  }

  static StreamContainer<T> stream<T>(List key, DbBaseType self) {
    var stringfiedPath = DbUtils.stringifyPath(key).join('/');

    if (!streams.containsKey(stringfiedPath)) {
      print("обьекта - нет - создаем: ${self.dbPath}");
      Future<EventSource> eventSource() async {
        var url = '$host/${stringfiedPath}.json'
            '${await _tokenPart()}';
        return await EventSource.connect(url,
            isWeb: db.isWeb, events: ['put', 'patch', 'auth_revoked']);
      }

      streams[stringfiedPath] = StreamContainer.async(stringfiedPath, self,
          eventSourceFuture: eventSource);
    }
    print("обьект есть - не создаем: ${self.dbPath}");
    return streams[stringfiedPath];
  }

  static Future<ResponseContainer> editOperationsSolver(List path, value,
      {DbEvent operation, isEtagNeeded: false}) async {
    if (value is Keywords && value != Keywords.timestamp.toString()) {
      value = (value as Keywords).short;
    } else if (value == Keywords.timestamp.toString()) {
      value = timestamp;
      // return value = timestamp;
    }

    ///Ищет первый пуш в операции
    var pushIndex = path.indexWhere(
            (key) => key is DbBaseType && (key.key?.contains(pushM) ?? false));

    ///Тут работаем со случаем пуша.
    if (pushIndex != -1 && operation != DbEvent.remove) {
      DbBaseType dbStringKey = path[pushIndex];
      var dbStringValue = dbStringKey.key;
      var isAsyncPushFired = false;
      if (!tmpKeysQueueMap.containsKey('${dbStringValue}')) {
        isAsyncPushFired = true;
        ///Первым делом мы формируем полноценный путь с нужными переменными
        if (pushIndex + 1 == path.length) {
          ///тот случай, когда пуш один и в самом конце.
          tmpKeysQueueMap['${dbStringValue}'] = _push(
              DbUtils.stringifyPath(path.sublist(0, path.length - 1)),
              value,
              isEtagNeeded);
        } else {
          //Данная часть кода переводит pushM в другой префикс,
          // чтобы при работе в локале не было желания у базы данных вновь запушить тот или иной путь.
              () {
            path
                .sublist(pushIndex + 1)
                .where((element) =>
            element is DbBaseType &&
                (element.key?.contains(pushM) ?? false))
                .forEach((element) {
              var dbBase = (element as DbBaseType);
              dbBase.key = dbBase.key.replaceFirst(pushM, 'lg');
            });
          }();

          var pathBeforePush = path.sublist(0, pushIndex);
          var pathAfterPush = path.sublist(pushIndex, path.length);
          var mergedValue = StreamContainer.path2map(
              DbUtils.stringifyPath(pathAfterPush).join('/'), value);
          tmpKeysQueueMap['${dbStringValue}'] = _push(
              DbUtils.stringifyPath(pathBeforePush), mergedValue, isEtagNeeded);
        }
      }
      var pushResponse = await tmpKeysQueueMap['${dbStringValue}'];
      dbStringKey.key = extractPushId(pushResponse);
      if(isAsyncPushFired) {
        databaseManager.localDatabaseManager
            .push2TruePath(dbStringKey, dbStringValue, dbStringKey.key);
        return ResponseContainer(
            pushResponse.statusCode,
            pushResponse.reasonPhrase,
            pushResponse.body,
            pushResponse.headers[Etag],
            DbEvent.completed);
      }
    }

    if (operation == DbEvent.remove) {
      var response = await _delete(DbUtils.stringifyPath(path));
      return ResponseContainer(response.statusCode, response.reasonPhrase,
          response.body, null, DbEvent.completed);
    }

    var response = await _update(DbUtils.stringifyPath(path), value);
    return ResponseContainer(response.statusCode, response.reasonPhrase,
        response.body, null, DbEvent.completed);
  }

  static Future<ResponseContainer> getOperationsSolver(List path,
      {isEtagNeeded: false}) async {
    var response = await _get(path, isEtagNeeded);
    var value = jsonDecode(response.body);
    return ResponseContainer(response.statusCode, response.reasonPhrase, value,
        response.headers[Etag], DbEvent.completed);
  }

  static Future<http.Response> _push(path, value, bool isEtagNeeded) async {
    var url = '$host/${path.join('/')}.json'
        '${await _tokenPart()}';
    var response = (await http.post(Uri.parse(url),
        body: jsonEncode(value), headers: isEtagNeeded ? ETAG_HEADER : null));
    return response;
  }

  static extractPushId(http.Response pushResponse) {
    return jsonDecode(pushResponse.body)['name'];
  }

  //TODO check timestamp.
  static const timestamp = {".sv": "timestamp"};

  static Future<http.Response> _putTimestamp(
      List path, bool isEtagNeeded) async {
    var url = '$host/${path.join('/')}.json'
        '${await _tokenPart()}';
    var response = (await http.put(Uri.parse(url),
        headers: isEtagNeeded ? ETAG_HEADER : null,
        body: jsonEncode({".sv": "timestamp"})));
    return response;
  }

  ///Пример обновления значения. [_update] Работает хорошо в случае, если мы загружаем не одно значение,
  ///а целый обьект, то-есть,
  /// он хорошо мержит.
  /// Например:
  // await RealtimeDatabaseManager.update([
  //     'test',
  //     'two',
  //     'cool'
  //   ], {
  //     'one': 'two',
  //     'three': 'two',
  //     'nice': {'soDeep': 'yeah'}
  //   });
  //
  //   await RealtimeDatabaseManager.update([
  //     'test',
  //     'two',
  //     'cool'
  //   ], {
  //     'one': '2',
  //   });
  //
  /// А уже так не сработает, т.к. у обьектов должен быть один путь.
  // await RealtimeDatabaseManager.update([
  //     'test',
  //     'two',
  //   ], {
  //     'cool': {
  //       'one': 'two',
  //       'three': '2',
  //
  //     }
  //   });
  static Future<http.Response> _update(List keys, rawValue) async {
    var value;
    var lastKey;
    if (rawValue == timestamp) {
      return await _putTimestamp(keys, false);
    }
    if (rawValue is! Map) {
      lastKey = keys.removeLast();
      value = {lastKey: rawValue};
    } else {
      value = rawValue;
    }
    var url = '$host/${keys.join('/')}.json'
        '${await _tokenPart()}';
    var response = await http.patch(Uri.parse(url), body: jsonEncode(value));
    return response;
  }

  ///Данный метод замещает обьект полностью.
  static Future<http.Response> _set(
      List keys, rawValue, bool isEtagNeeded) async {
    var value;
    var lastKey;
    if (rawValue is! Map) {
      lastKey = keys.removeLast();
      value = {lastKey: rawValue};
    } else {
      value = rawValue;
    }
    var url = '$host/${keys.join('/')}.json'
        '${await _tokenPart()}';
    var response = await http.put(Uri.parse(url),
        body: jsonEncode(value), headers: isEtagNeeded ? ETAG_HEADER : null);
    return response;
  }

  static Future<http.Response> _get(List path, bool isEtagNeeded) async {
    var url = '$host/${DbUtils.stringifyPath(path).join('/')}.json'
        '${await _tokenPart()}';
    var response = await http.get(Uri.parse(url),
        headers: isEtagNeeded ? ETAG_HEADER : null);
    return response;
  }

  static Future<bool> isEtagDidntChanged(List path, String etag) async {
    var response = await _delete(path, ifMatch: 'fake_etag');
    return response.headers[Etag] == etag;
  }

  static Future<http.Response> _delete(List path, {String ifMatch}) async {
    var url = '$host/${DbUtils.stringifyPath(path).join('/')}.json'
        '${await _tokenPart()}';
    var response = await http.delete(Uri.parse(url),
        headers: {if (ifMatch != null) EtagIfMatch: ifMatch});
    return response;
  }

  static Map<String, StreamContainer> streams = {};
}
