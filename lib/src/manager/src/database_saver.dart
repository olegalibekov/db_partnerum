import '../../../db_partnerum.dart';
import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:xxtea/xxtea.dart';

class SaveOperation {}

class Saver {
  Saver() {
    saveToLocalQueStream = saveToLocalQue.stream.asBroadcastStream();
  }

  saveToLocal(Root localRoot) {
    var _currentSaveOp = SaveOperation();
    if (root == localRoot) {
      next = _currentSaveOp;
    }
    if (_savingProcess == null) {
      _savingProcess = save();
    } else {
      saveToLocalQueStream.first.then((value) {
        if (_currentSaveOp == next) {
          _savingProcess = save();
        }
      });
    }
  }

  save() async {
    var key = db.localKey.hashCode.toString();
    //rootMaps[root] != null && rootMaps[root].isNotEmpty;
    var value = jsonEncode(LocalDatabaseManager.rootMaps[root]);
    final encrypted = xxtea.encryptToString(value, db.localKey);
    var isSavedLocally = await db.saver(key, encrypted);
    if (isSavedLocally) print('saved locally');
    saveToLocalQue.add(next);
    _savingProcess = null;
  }

  SaveOperation next;
  var saveToLocalQue = StreamController<SaveOperation>();
  Stream saveToLocalQueStream;
  Future _savingProcess;
}
