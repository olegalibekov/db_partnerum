import 'package:db_partnerum/db_partnerum.dart';

import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:mime/mime.dart';
import 'package:path/path.dart';

getGoogleCloudUploadUrl(String bucketName, String filePath) {
  return 'https://storage.googleapis.com/upload/storage/v1/b/$bucketName/o?uploadType=media&name=$filePath';
}

getCloudDeleteUrl(String bucketName, String filePath) {
  var encodedImagePath = Uri.encodeQueryComponent(filePath);
//  var imageName = filePath.contains("/") ? basename(filePath) : filePath;
  return 'https://storage.googleapis.com/storage/v1/b/$bucketName/o/$encodedImagePath';
}

getFirebaseDownloadUrl(String bucketName, String filePath) {
  var encodedImagePath = Uri.encodeQueryComponent(filePath);
  return "https://firebasestorage.googleapis.com/v0/b/$bucketName/o/$encodedImagePath?alt=media";
}

extractPathFromFirebaseUrl(String firebaseStorageUrl) {
  String raw = basename(firebaseStorageUrl).split("?").first;
//  return raw;
  return Uri.decodeComponent(raw);
}

Future<Uint8List> readGoogleCloudImage(
    String bucketName, String imagePath, String accessToken) async {
//    var imagePath = 'images/fe3fc2b5-4e8f-4d00-9c51-a32b50a86e0d.jpg';
//  var encodedImagePath = Uri.encodeQueryComponent(imagePath);
  var uri = Uri.parse(
      GStorageHelper._getGoogleCloudDownloadUrl(bucketName, imagePath));

//  client.close();
  return http.readBytes(uri, headers: <String, String>{
    'Authorization': "Bearer $accessToken",
  });
}

Future<http.Response> deleteImageFromGoogleCloud(
    String bucketName, String firebaseStorageUrl, String accessToken) async {
  var storagePath = extractPathFromFirebaseUrl(
    firebaseStorageUrl,
  );
  print("Delete path is $storagePath");
  var deleteCloudUrl = getCloudDeleteUrl(bucketName, storagePath);
  print("Delete Url is $deleteCloudUrl");
  var uri = Uri.parse(deleteCloudUrl);
  return await http.delete(
    uri,
    headers: <String, String>{
      'Authorization': "Bearer $accessToken",
//      'Content-Type': "image/jpeg"
    },
  );
}

isPathUrl(String path) {
  var urlPattern =
      r"(https?|http)://([-A-Z0-9.]+)(/[-A-Z0-9+&@#/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#/%=~_|!:‌​,.;]*)?";
  var firstMatch = new RegExp(urlPattern, caseSensitive: false);
  return path.contains(firstMatch);
}

String uniqueStoragePath(String path, {String folder}) {
  return "${folder != null ? "$folder/" : ""}${path.split("/").last}-${Random().nextInt(3243539)}.jpg";
}

class GStorageHelper {
  static const _FIREBASE_HOST = 'firebasestorage.googleapis.com';

  static get bucketName => db.storageRoot ?? '${db.projectId}.appspot.com';

  static uploadFile(FileWrapper fileWrapper, String path) async {
    var generatedFilename =
        '${DateTime.now().millisecondsSinceEpoch}_${Random().nextInt(100000)}${extension(fileWrapper.fileName)}';
    var encodedPath = '$path/$generatedFilename';
    var response = await _uploadByte2Cloud(bucketName, encodedPath,
        fileWrapper.bytes, lookupMimeType(fileWrapper.fileName));
    return generatedFilename;
  }

  static String url(String fileName, String path,
      {bool forceFirebaseLink: false}) {
    //var _link = await link(bucketName, encodedPath);
    // var links =
    //     'https://$_FIREBASE_HOST/v0/b/$bucketName/o/$encodedPath?create_token=true';
    // var tokenRequest = await http.get(links, headers: await httpHeaders);
    //print(tokenRequest);
    //return _link;

    String url;
    if (db.realtimeIdTokenGeneratorServer != null &&
        forceFirebaseLink == false) {
      url = _getGoogleCloudDownloadUrl(bucketName, '$path/$fileName');
      //сервер
    } else {
      url = getFirebaseDownloadUrl(bucketName, '$path/$fileName');
      // var tokenRequestUrl =
      //     getFirebaseDownloadUrl(bucketName, '$path/$fileName');
      // var tokenResponse = await http.get(tokenRequestUrl,
      //     headers: {'Authorization': authorizationHeader});
      // print(tokenResponse.body);
      // var response = jsonDecode(tokenResponse.body);
      // print(response);
      //флаттер
    }
    return url;
  }

  firebaseUploader() {}

  googleUploader() {}

  static Future<http.Response> _uploadByte2Cloud(String bucketName,
      String filePath, Uint8List byteData, contentType) async {
    String accessToken = await DbUtils.token();
    Uri uri;
    String authorizationHeader;
    if (db.realtimeIdTokenGeneratorServer != null) {
      ///загрузка через google cloud
      uri = Uri.parse(getGoogleCloudUploadUrl(bucketName, filePath));
      authorizationHeader = 'Bearer $accessToken';
    } else {
      ///загрузка через firebase
      uri = Uri.parse(
          'https://${_FIREBASE_HOST}/v0/b/${bucketName}/o?name=$filePath');
      authorizationHeader = 'Firebase $accessToken';
    }
    var response = await http.post(uri,
        headers: <String, String>{
          'Authorization': authorizationHeader,
          'Content-Type': contentType,
          'Content-length': '${byteData.length}'
        },
        body: byteData);
    return response;
  }

  //{
  //   "kind": "storage#object",
  //   "id": "golden-memory-243607.appspot.com/user_data/test/houses/tets/house_information/photos/-MVabDykKfasV5FFBg1f/README.txt/1615615648216240",
  //   "selfLink": "https://www.googleapis.com/storage/v1/b/golden-memory-243607.appspot.com/o/user_data%2Ftest%2Fhouses%2Ftets%2Fhouse_information%2Fphotos%2F-MVabDykKfasV5FFBg1f%2FREADME.txt",
  //   "mediaLink": "https://storage.googleapis.com/download/storage/v1/b/golden-memory-243607.appspot.com/o/user_data%2Ftest%2Fhouses%2Ftets%2Fhouse_information%2Fphotos%2F-MVabDykKfasV5FFBg1f%2FREADME.txt?generation=1615615648216240&alt=media",
  //   "name": "user_data/test/houses/tets/house_information/photos/-MVabDykKfasV5FFBg1f/README.txt",
  //   "bucket": "golden-memory-243607.appspot.com",
  //   "generation": "1615615648216240",
  //   "metageneration": "1",
  //   "contentType": "text/plain",
  //   "storageClass": "STANDARD",
  //   "size": "2462",
  //   "md5Hash": "s/ALuCRicQ9lUld5i+5WBQ==",
  //   "crc32c": "4IIAyQ==",
  //   "etag": "CLDpuKbNrO8CEAE=",
  //   "timeCreated": "2021-03-13T06:07:28.335Z",
  //   "updated": "2021-03-13T06:07:28.335Z",
  //   "timeStorageClassUpdated": "2021-03-13T06:07:28.335Z"
  // }

//{
//   "name": "test.some",
//   "bucket": "golden-memory-243607.appspot.com",
//   "generation": "1615615478361592",
//   "metageneration": "1",
//   "contentType": "image/png",
//   "timeCreated": "2021-03-13T06:04:38.480Z",
//   "updated": "2021-03-13T06:04:38.480Z",
//   "storageClass": "STANDARD",
//   "size": "46822",
//   "md5Hash": "ja4Mkhx/BL38JSGX73EC+g==",
//   "contentEncoding": "identity",
//   "contentDisposition": "inline; filename*=utf-8''test.some",
//   "crc32c": "ax6+eA==",
//   "etag": "CPjbudXMrO8CEAE=",
//   "downloadTokens": "574c65bb-4045-4d78-9591-75c156aa57ea"
// }
  static String link(bucketName, encodedPath) {
    var linkGetter = _getGoogleCloudDownloadUrl(bucketName, encodedPath);
    return linkGetter;
  }

  static String _getGoogleCloudDownloadUrl(String bucketName, String filePath) {
    var encodedImagePath = Uri.encodeQueryComponent(filePath);
    return 'https://storage.googleapis.com/storage/v1/b/$bucketName/o/$encodedImagePath?alt=media';
  }

  static Future<Uint8List> fileBytes(String fileName, String path) async {
    String accessToken = await DbUtils.token();
    String authorizationHeader = db.realtimeIdTokenGeneratorServer != null
        ? 'Bearer $accessToken'
        : 'Firebase $accessToken';
    String url = GStorageHelper.url(fileName, path);

    var link = await http
        .get(Uri.parse(url), headers: {'Authorization': authorizationHeader});
    if (link.statusCode == 200) {
      return link.bodyBytes;
    }
    return null;
  }

  static Future<String> urlWithToken(String fileName, String path) async {
    String accessToken = await DbUtils.token();
    String authorizationHeader = db.realtimeIdTokenGeneratorServer != null
        ? 'Bearer $accessToken'
        : 'Firebase $accessToken';
    String url = GStorageHelper.url(fileName, path, forceFirebaseLink: true);

    String filePath = '$path/$fileName';
    var response = await http.get(
        Uri.parse(
            'https://${_FIREBASE_HOST}/v0/b/${bucketName}/o?name=$filePath'),
        headers: <String, String>{'Authorization': authorizationHeader});
    if (response.statusCode == 200) {
      var fileInfo = jsonDecode(response.body);
      var token = fileInfo['downloadTokens'];
      if (token is String) {
        return '$url&token=$token';
      }
    }
    return null;
  }

  /// true for successful
  /// false for fail
  static Future<bool> delete(String fileName, String path) async {
    String accessToken = await DbUtils.token();
    String authorizationHeader = db.realtimeIdTokenGeneratorServer != null
        ? 'Bearer $accessToken'
        : 'Firebase $accessToken';

    String filePath = '$path/$fileName';
    var response = await http.delete(
        Uri.parse(
            'https://${_FIREBASE_HOST}/v0/b/${bucketName}/o?name=$filePath'),
        headers: <String, String>{'Authorization': authorizationHeader});
    if (response.statusCode == 204) {
      return true;
    }
    return false;
  }

  static Future<Map<String, String>> get httpHeaders async {
    String accessToken = await DbUtils.token();
    String authorizationHeader = db.realtimeIdTokenGeneratorServer != null
        ? 'Bearer $accessToken'
        : 'Firebase $accessToken';
    return <String, String>{'Authorization': authorizationHeader};
  }

  static Map<String, String> get syncHttpHeaders {
    String authorizationHeader = db.realtimeIdTokenGeneratorServer != null
        ? 'Bearer ${DbUtils.cachedToken}'
        : 'Firebase ${DbUtils.cachedToken}';
    return <String, String>{'Authorization': authorizationHeader};
  }

  static Future<http.Response> deleteImageFromStorage(
      String bucketName, String firebaseStorageUrl) async {
    String accessToken = await DbUtils.token();
    String authorizationHeader = db.realtimeIdTokenGeneratorServer != null
        ? 'Bearer $accessToken'
        : 'Firebase $accessToken';
    var storagePath = extractPathFromFirebaseUrl(firebaseStorageUrl);
    print("Delete path is $storagePath");
    var deleteCloudUrl = getCloudDeleteUrl(bucketName, storagePath);
    print("Delete Url is $deleteCloudUrl");
    var uri = Uri.parse(deleteCloudUrl);
    return await http.delete(
      uri,
      headers: <String, String>{'Authorization': authorizationHeader},
    );
  }
}
