import 'package:db_partnerum/db_partnerum.dart';

import 'dart:async';
import 'dart:convert';

import 'package:async/async.dart';
import 'package:eventsource/eventsource.dart';
import 'package:micro_utils/merge_map.dart';

typedef EventSourceFuture = Future<EventSource> Function();

class StreamContainer<T> {
  final StreamGroup streamGroup = StreamGroup();
  final DbBaseType baseObject;
  Stream<T> stream;
  final baseValue;
  var lastPortion;
  EventSource eventSource;
  final path;
  Future _initializationFuture;

  onCancel(StreamSubscription<T> sb) async {
    print("начинаю выход из: ${baseObject.dbPath}");
    RealtimeDatabaseManager.streams.remove(this.path);
    if (eventSource == null) {
      await _initializationFuture;
    }
    await eventSource.client.close();
    print("закончен выход из: ${baseObject.dbPath}");
  }

  // static async<T>(path, {Future<EventSource> eventSource, baseValue}) async =>
  //     StreamContainer(path,
  //         eventSource: await eventSource, baseValue: baseValue);

  StreamContainer.async(this.path, this.baseObject,
      {EventSourceFuture eventSourceFuture, this.baseValue}) {
    stream = streamGroup.stream.asBroadcastStream(onCancel: this.onCancel);
    print("старт входа в async: ${baseObject.dbPath}");
    if (db.streamCacheOn) {
      print('root: ${LocalDatabaseManager.rootMaps[root]}');
      var _tmpObj = databaseManager.localDatabaseManager[baseObject];
      if (_tmpObj != null) {
        print('here is yoour local copy: $_tmpObj');
        streamGroup.add(Stream.value(_tmpObj));
      }
    }

    _initializationFuture = () async {
      this.eventSource = await eventSourceFuture();
      streamGroup.add(eventSource
          .where((event) =>
      event != null && event.data != null && event.data != 'null')
          .map<T>((event) {
        var mapRepresentation = jsonDecode(event.data);
        var data =
        path2map(mapRepresentation['path'], mapRepresentation['data']);
        if (data is Map) {
          Map _lastP = lastPortion as Map;
          _lastP = mergeMap([_lastP, data], acceptNull: true, removeNull: true);
          lastPortion = _lastP;
          _lastP = null;
        } else {
          lastPortion = data;
        }
        return lastPortion;
      }));
    }();
  }

  static path2map(String rawPath, value) {
    var path = rawPath.split('/');
    insider(List path, value) {
      if (path.length == 1)
        return {path.first: value};
      else
        return {path.removeAt(0): insider(path, value)};
    }

    if (path[1].isEmpty) {
      return value;
    } else {
      path.removeAt(0);
      return insider(path, value);
    }
  }
}

class StreamUtils {
  static Stream<T> streamBasedOnFuture<T extends DbBaseType>(
      T value, Future future) {
    var streamGroup = StreamGroup<T>();
    var _tmpObj = databaseManager.localDatabaseManager[value];
    if (_tmpObj != null) {
      print('here is your local copy: $_tmpObj');
      streamGroup.add(Stream.value(value));
    }
    streamGroup.add(future.asStream());
    return streamGroup.stream;
  }
}
