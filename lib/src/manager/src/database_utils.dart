import '../../././../db_partnerum.dart';
import 'package:event_container/event_container.dart';

abstract class DbUtils {
  static String cachedToken;

  static Future<String> token() async {
    cachedToken = await (db.realtimeIdTokenGenerator ??
        db.realtimeIdTokenGeneratorServer)();
    return cachedToken;
  }

  static List stringifyPath(List path) {
    var isContainsB = false;
    var stringifiedPath = path.map((key) {
      if (key is Keywords) {
        return key.short;
      }
      if (key is DbBaseType) {
        if (!key.runtimeType.toString().endsWith('Map')) {
          if (key.key.contains('-b')) {
            isContainsB = true;
            // return key.value.replaceAll('-b', '');
          }
        }
        if (key == pushM) {
          throw 'DbString contains _push';
        }
        return key.key;
      } else {
        return key;
      }
    }).toList();
    if (isContainsB) {
      stringifiedPath.insert(0, 'b');
    }
    return stringifiedPath;
  }

  static List<DbBaseType> before2dbPath(DbBaseType object) {
    var tmpObj = object;
    List<DbBaseType> types = [];
    while (tmpObj != null) {
      types.add(tmpObj);
      tmpObj = tmpObj.before;
    }
    return types.reversed.toList();
  }

  ///Данная функция value на полноценное присваивание и также говорит о том, что операция - сет.
  setSolver() {
    //users.set(value)
  }

  static operationsListImplementer(List operationsList,
      EventContainer<DbEvent, DbSetPair<dynamic>> task) async {
    operationsList.add(task);
    await DbUtils.taskCompleteWaiter(task);
    operationsList.remove(task);
  }

  static Future<DbEvent> taskCompleteWaiter(EventContainer task) async {
    await databaseManager.operations.firstWhere(
            (element) => element == task && task.eventType == DbEvent.completed);
    return DbEvent.completed;
  }

  static bool isDbBaseTypeSimple(DbBaseType type) =>
      type is! DbString &&
          type is! DbDateTime &&
          type is! DbDuration &&
          type is! DbNumber &&
          type is! DbObj;
}

class DbSetPair<T> {
  final DbBaseType baseType;
  final T value;

  DbSetPair(this.baseType, [this.value]) : assert(baseType != null);

  @override
  String toString() {
    return '$DbSetPair($baseType, $value)';
  }
}
