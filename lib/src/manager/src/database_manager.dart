import '../../../db_partnerum.dart';
import 'package:db_partnerum/src/manager/manager.dart';
import 'package:db_partnerum/src/models/native_types/types.dart';
import 'package:db_partnerum/src/manager/src/settings_manager.dart';
import 'dart:async';

import 'package:async/async.dart';
import 'package:event_container/event_container.dart';

// ToDo get only changed data
// ToDO 3 different classes (with static methods?) to interact with each DB(Firestore, RDB, Local)
// Local DB has an encryption based on user ID
// Write to local DB when no Internet connection. When internet appears -> write saved data to internet DB

final String pushM = '_push';

/// ![example](https://pub.dev/static/img/dart-logo.svg?hash=gqea88bp2ii6tqf5rn5p8jtg22mma809)
class DatabaseManager {
  static final DatabaseManager _singleton = DatabaseManager._internal();
  final localDatabaseManager = LocalDatabaseManager();
  final sessionManager = SessionManager();

  final StreamController managerPaparazzi = StreamController();
  final StreamController<EventContainer<DbEvent, DbSetPair>> operationsFlow =
  StreamController();

  ///Дает возможность слушать какую-то определенную информаицю от базы данных -
  ///например её текущее состояние - загрузка или не загрузка
  ///Причем должно быть деление на типы ивентов - фоновые и реакция действие пользователя

  ///1. говорить о том, что мы оффлайн - сорри
  ///

  Stream paparazziFlow;
  Stream<EventContainer<DbEvent, DbSetPair>> operations;

  Stream<T> stream<T extends DbBaseType>(List key, DbBaseType self) {
    var streamContainer = RealtimeDatabaseManager.stream(key, self);
    var stream = streamContainer.stream.map<T>((event) {
      localDatabaseManager.write(self, event, type: OperationType.update);
      return self;
    });
    if (streamContainer.lastPortion != null) {
      localDatabaseManager.write(self, streamContainer.lastPortion,
          type: OperationType.update);
      var lastPortionStream = Stream<T>.value(self);
      return StreamGroup.merge<T>([lastPortionStream, stream]);
    }
    return stream;
  }

  List pathTasks(DbBaseType object) {
    return [];
  }

  get(DbBaseType object) => localDatabaseManager[object];

  set(DbBaseType object, value) =>
      localDatabaseManager.write(object, value, type: OperationType.merge);

  checkRequest({List array = const [], value}) {
    for (var element in array) {
      if (!(element is Keywords)) {
        return false;
      }
    }
    if (!(value is Keywords)) {
      return false;
    }
    return true;
  }

  setData(String key, dynamic value) {}

  factory DatabaseManager() {
    return _singleton;
  }

  StreamSubscription<EventContainer> operationsWorker;

  _filterCompletedTasks(EventContainer task) =>
      task.eventType != DbEvent.completed;

  _valuesUploader(EventContainer task) async {
    if (task.eventType == DbEvent.upload) {
      var lastTask;
      try {
        lastTask = this.packetTasks.last;
      } catch (e) {}

      this.packetTasks.add(task);
      if (lastTask != null) {
        await DbUtils.taskCompleteWaiter(lastTask);
      }
      var tasks = taskBunch[task.eventValue.baseType]?.toSet();
      if (tasks != null && tasks.isNotEmpty) {
        for (var tmpTask in tasks) {
          await taskCompleter(tmpTask);
          taskBunch[task.eventValue.baseType].remove(tmpTask);
        }
      }
      this.packetTasks.remove(task);
      operationsFlow.add(task..eventType = DbEvent.completed);
    }
  }

  ///Первое, что происходит при инициализации - загрузка статистических данных юзера.
  DatabaseManager._internal() {
    // Firestore.initialize(db.projectId).collection(path).get();
    // var firebaseAuth = FirebaseAuth.initialize('', null);
    // wait firebaseAuth.signIn(email, password);
    paparazziFlow = managerPaparazzi.stream.asBroadcastStream();
    operations = operationsFlow.stream.asBroadcastStream();
    operationsWorker = operations
        .where((task) => _filterCompletedTasks(task))
        .listen((task) async {
      ///Этот код проверяет задачу, на ее причастие к операции формата upload, это операция,
      /// которая аккумулирует все пемеренные. В случе принадлежности список пополняется и откладывается на потом,
      /// иначе задача выполняется сразу.
      if (task.eventType == DbEvent.set ||
          task.eventType == DbEvent.get ||
          task.eventType == DbEvent.remove) {
        if (!bunchCollector(task)) await taskCompleter(task);
      }

      ///Данная часть кода обрабатывает задачу формата upload,
      ///триггер для собранных задач.
      await _valuesUploader(task);

      //Слушаем завершение операции;
      if (task.eventType == DbEvent.close) {
        operationsFlow.close();
        operationsWorker.cancel();
      }
    });
  }

  ///Требуется стак операци - стак микро транзакций;  умный стак,который микротранзации оптимизирует внутри;
  ///перезапись обьекта - делаем микро записи, и их обновлять не надо
  ///Пакеты - пакет микро транзацкицй; больший пакет
  ///Нижний путь кладем в очередь, после дочерних путей.

  ///если запрос уже существует и кто-то еще хочет получить то же значение, то мы подписываемся на него ?
  ///(но только в том случае если запрос не начал выполняться)
  ///
  ///
  /// доп переменная, которая позволит сделать загрузку послдеовательной для всех патчей

  List<EventContainer<DbEvent, DbSetPair<dynamic>>> tasks = [];
  List packetTasks = [];
  Map<DbBaseType, Set<EventContainer<DbEvent, DbSetPair<dynamic>>>> taskBunch =
  {};

  toBunch(DbBaseType object, EventContainer<DbEvent, DbSetPair<dynamic>> task) {
    if (!taskBunch.containsKey(object)) {
      taskBunch[object] = Set();
    } else {
      //print(taskBunch[object]?.first == task);
    } // var set = taskBunch.putIfAbsent(object, () => Set());
    taskBunch[object].add(task);
    //print(taskBunch[object]);
  }

  bunchCollector(EventContainer<DbEvent, DbSetPair<dynamic>> task) {
    var baseType = task.eventValue.baseType;
    if (baseType is DbObj && baseType.uploadComplex) {
      return false;
    }
    var path = baseType.dbPath;
    for (var key in path) {
      if (key.settings.packet.isOn()) {
        toBunch(key, task);
        return true;
      }
    }
    return false;
  }

  taskCompleter(EventContainer<DbEvent, DbSetPair<dynamic>> task) async {
    var object = task.eventValue;
    var baseType = object.baseType;
    var lastTask;

    ///Работает в том случае, если включена оптимизация
    if (db.optimizationMode ||
        baseType.dbPath
            .any((element) => element.settings.optimization.isOn())) {
      var operationsList = baseType.operationsList;
      var lastOperation = operationsList.lastWhere(
              (operation) => operation.eventType == task.eventType,
          orElse: () => null);

      if (lastOperation != task) {
        this.tasks.remove(task);
        DbUtils.taskCompleteWaiter(lastOperation).then((value) {
          operationsFlow.add(task..eventType = DbEvent.completed);
        });
        return;
      }
    }
    var completeSettings = DbBaseTypeSettings.completeSettings(baseType);
    if (!completeSettings.async.isOn() && !db.asyncMode) {
      try {
        lastTask = this.tasks.last;
      } catch (e) {}

      this.tasks.add(task);
      if (lastTask != null) {
        await DbUtils.taskCompleteWaiter(lastTask);
      }
    } else {
      var operationsList = baseType.operationsList;
      if (operationsList.last != task) {
        var currentOperationIndex = operationsList.indexOf(task, -1);
        if (currentOperationIndex != -1 && currentOperationIndex - 1 >= 0) {
          var lastOperation = operationsList[currentOperationIndex - 1];
          if (lastOperation != null) {
            await DbUtils.taskCompleteWaiter(lastOperation);
          }
        }
      }
    }

    if (baseType is DbObj && !baseType.uploadComplex) {
      baseType.uploadComplex = true;
    }
    if (task.eventType == DbEvent.set) {
      await valueSetter(object);
    }
    if (task.eventType == DbEvent.get) await valueGetter(object);
    if (task.eventType == DbEvent.remove) await valueRemover(object);
    this.tasks.remove(task);
    operationsFlow.add(task..eventType = DbEvent.completed);
  }

  base2json() {}

  json2base() {}

  valueGetter(DbSetPair pair) async {
    // if(pair);
    var path = pair.baseType.dbPath;
    path.removeAt(0);
    if (db.etagCacheOn && pair.baseType.etag.isNotEmpty) {
      print('path: $path - checking etag: ${pair.baseType.etag}');
      var isEtagDidntChanged = await RealtimeDatabaseManager.isEtagDidntChanged(
          path, pair.baseType.etag);
      if (isEtagDidntChanged) {
        print('path: $path - value:${localDatabaseManager[pair.baseType]}');
        return;
      }
    }
    var responseContainer = await RealtimeDatabaseManager.getOperationsSolver(
        path,
        isEtagNeeded: DbUtils.isDbBaseTypeSimple(pair.baseType));
    var value = responseContainer.value;
    // print(value);
    if (pair.baseType is DbString) {}
    if (pair.baseType is DbNumber) {}
    if (pair.baseType is DbBool) {}
    if (pair.baseType is DbDuration) {}
    if (pair.baseType is DbDateTime) {}
    if (pair.baseType is DbMap) {}
    if (pair.baseType is DbList) {}
    if (pair.baseType is DbObj) {}
        () {
      localDatabaseManager.write(pair.baseType, value,
          type: OperationType.update, eTag: responseContainer.etag);
      // LocalDatabaseManager.wayThrough(pair.baseType, value);
    }();
    print('path: $path - value:${localDatabaseManager[pair.baseType]}');
  }

  valueSetter(DbSetPair pair) async {
    var path = pair.baseType.dbPath.toList();
    path.removeAt(0);
    var value = await RealtimeDatabaseManager.editOperationsSolver(
        path, pair.value,
        operation: DbEvent.set,
        isEtagNeeded: DbUtils.isDbBaseTypeSimple(pair.baseType));
    if (value != null) {
      localDatabaseManager.write(pair.baseType, pair.value,
          type: OperationType.merge, eTag: value.etag);
    }
    print('path: $path - value:${pair.value}');
  }

  valueRemover(DbSetPair pair) async {
    var path = pair.baseType.dbPath.toList();
    path.removeAt(0);
    var value = await RealtimeDatabaseManager.editOperationsSolver(
        path, pair.value,
        operation: DbEvent.remove,
        isEtagNeeded: DbUtils.isDbBaseTypeSimple(pair.baseType));
    if (value != null) {
      localDatabaseManager.write(pair.baseType, null,
          type: OperationType.remove);
    }
    print('path: $path - remove');
  }
}

// Set<DbBaseType> getAllObjects(DbBaseType object) {
//   var set = LocalDatabaseManager.rootMaps[object];
//   var tmpSet = Set<DbBaseType>();
//   for (var i in set) {
//     tmpSet.addAll(getAllObjects(object));
//   }
//   return set..addAll(tmpSet);
// }
