import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class OfferObject extends DbBaseType{
OfferObject([String key]) : super(key);

Chat get chat => get(Chat('chat'));
set chat(value) => chat.set(value);
ConversationUsers get conversationUsers => get(ConversationUsers('conversation_users'));
set conversationUsers(value) => conversationUsers.set(value);
RequestInformation get requestCopy => get(RequestInformation('request_copy'));
set requestCopy(value) => requestCopy.set(value);
HouseInformation get houseCopy => get(HouseInformation('house_copy'));
set houseCopy(value) => houseCopy.set(value);
DbString get status => get(DbString('status'));
set status(value) => status.set(value);
Review get review => get(Review('review'));
set review(value) => review.set(value);
DbString get requestId => get(DbString('request_id'));
set requestId(value) => requestId.set(value);
@override

Future<OfferObject> get future async => await super.future as OfferObject;
@override

Stream<OfferObject> get stream => super.getStream<OfferObject>();
@override

Stream<OfferObject> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  OfferObject get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<OfferObject> set(value) async {
    if (value is OfferObject) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
