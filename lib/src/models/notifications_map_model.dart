import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class NotificationsMap extends DbMap<DbString, UserNotification> {    NotificationsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<NotificationsMap> get future async =>
      await super.future as NotificationsMap;
      
  @override
  Stream<NotificationsMap> get stream => super.getStream<NotificationsMap>();
  
  @override
  Stream<NotificationsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  NotificationsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<NotificationsMap> set(value, [object]) async {
    if (value is NotificationsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
