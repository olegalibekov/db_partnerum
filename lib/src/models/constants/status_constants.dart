class StatusConstants {
  // Agent
  static const houseRequestSended = 'house_request_sended';
  static const houseRequestCanceled = 'house_request_canceled';

  // Common
  static const requestBooked = 'request_booked';

  // Owner
  static const requestAccept = 'request_accept';
  static const requestDenied = 'request_denied';
}
