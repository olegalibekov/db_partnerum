import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class UsersStatuses extends DbBaseType{
UsersStatuses([String key]) : super(key);

DbString get requestUserStatus => get(DbString('request_user_status'));
set requestUserStatus(value) => requestUserStatus.set(value);
DbString get ownerUserStatus => get(DbString('owner_user_status'));
set ownerUserStatus(value) => ownerUserStatus.set(value);
@override

Future<UsersStatuses> get future async => await super.future as UsersStatuses;
@override

Stream<UsersStatuses> get stream => super.getStream<UsersStatuses>();
@override

Stream<UsersStatuses> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  UsersStatuses get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UsersStatuses> set(value) async {
    if (value is UsersStatuses) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
