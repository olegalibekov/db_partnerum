import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class ConversationUsers extends DbBaseType{
ConversationUsers([String key]) : super(key);

DbString get requestUser => get(DbString('request_user'));
set requestUser(value) => requestUser.set(value);
DbString get ownerUser => get(DbString('owner_user'));
set ownerUser(value) => ownerUser.set(value);
DbString get chatId => get(DbString('chat_id'));
set chatId(value) => chatId.set(value);
UsersStatuses get usersStatuses => get(UsersStatuses('users_statuses'));
set usersStatuses(value) => usersStatuses.set(value);
DbString get bookingDatesId => get(DbString('booking_dates_id'));
set bookingDatesId(value) => bookingDatesId.set(value);
@override

Future<ConversationUsers> get future async => await super.future as ConversationUsers;
@override

Stream<ConversationUsers> get stream => super.getStream<ConversationUsers>();
@override

Stream<ConversationUsers> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  ConversationUsers get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<ConversationUsers> set(value) async {
    if (value is ConversationUsers) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
