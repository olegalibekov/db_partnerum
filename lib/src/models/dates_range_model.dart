import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class DatesRange extends DbBaseType{
DatesRange([String key]) : super(key);

DbDuration get first => get(DbDuration('first'));
set first(value) => first.set(value);
DbDuration get last => get(DbDuration('last'));
set last(value) => last.set(value);
@override

Future<DatesRange> get future async => await super.future as DatesRange;
@override

Stream<DatesRange> get stream => super.getStream<DatesRange>();
@override

Stream<DatesRange> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  DatesRange get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<DatesRange> set(value) async {
    if (value is DatesRange) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
