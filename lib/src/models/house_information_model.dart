import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class HouseInformation extends DbBaseType{
HouseInformation([String key]) : super(key);

DbString get address => get(DbString('address'));
set address(value) => address.set(value);
DbString get city => get(DbString('city'));
set city(value) => city.set(value);
DbString get description => get(DbString('description'));
set description(value) => description.set(value);
DbDuration get timeToMetro => get(DbDuration('time_to_metro'));
set timeToMetro(value) => timeToMetro.set(value);
PhotosMap get photos => get(PhotosMap('photos', (key) => DbString(key)));
set photos(value) => photos.set(value);
PriceRange get priceRange => get(PriceRange('price_range'));
set priceRange(value) => priceRange.set(value);
DbNumber<int> get rooms => get(DbNumber<int>('rooms'));
set rooms(value) => rooms.set(value);
StationsMap get stations => get(StationsMap('stations', (key) => DbString(key)));
set stations(value) => stations.set(value);
DbBool get isArchived => get(DbBool('is_archived'));
set isArchived(value) => isArchived.set(value);
@override

Future<HouseInformation> get future async => await super.future as HouseInformation;
@override

Stream<HouseInformation> get stream => super.getStream<HouseInformation>();
@override

Stream<HouseInformation> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  HouseInformation get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<HouseInformation> set(value) async {
    if (value is HouseInformation) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
