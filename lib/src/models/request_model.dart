import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Request extends DbBaseType{
Request([String key]) : super(key);

DbDuration get timestamp => get(DbDuration('timestamp'));
set timestamp(value) => timestamp.set(value);
RequestInformation get requestInformation => get(RequestInformation('request_information'));
set requestInformation(value) => requestInformation.set(value);
OffersMap get offers => get(OffersMap('offers', (key) => Offer(key)));
set offers(value) => offers.set(value);
HouseOffersMap get houseOffers => get(HouseOffersMap('house_offers', (key) => DbString(key)));
set houseOffers(value) => houseOffers.set(value);
@override

Future<Request> get future async => await super.future as Request;
@override

Stream<Request> get stream => super.getStream<Request>();
@override

Stream<Request> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Request get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Request> set(value) async {
    if (value is Request) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
