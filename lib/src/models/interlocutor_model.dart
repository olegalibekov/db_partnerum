import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Interlocutor extends DbBaseType{
Interlocutor([String key]) : super(key);

DbString get status => get(DbString('status'));
set status(value) => status.set(value);
DbString get companionId => get(DbString('companion_id'));
set companionId(value) => companionId.set(value);
UserInformation get companionInfo => get(UserInformation('companion_info'));
set companionInfo(value) => companionInfo.set(value);
@override

Future<Interlocutor> get future async => await super.future as Interlocutor;
@override

Stream<Interlocutor> get stream => super.getStream<Interlocutor>();
@override

Stream<Interlocutor> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Interlocutor get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Interlocutor> set(value) async {
    if (value is Interlocutor) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
