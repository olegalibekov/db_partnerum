import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class MessagesMap extends DbMap<DbString, Message> {    MessagesMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<MessagesMap> get future async =>
      await super.future as MessagesMap;
      
  @override
  Stream<MessagesMap> get stream => super.getStream<MessagesMap>();
  
  @override
  Stream<MessagesMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  MessagesMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<MessagesMap> set(value, [object]) async {
    if (value is MessagesMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
