import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class GlobalContactsMap extends DbMap<DbString, DbString> {    GlobalContactsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<GlobalContactsMap> get future async =>
      await super.future as GlobalContactsMap;
      
  @override
  Stream<GlobalContactsMap> get stream => super.getStream<GlobalContactsMap>();
  
  @override
  Stream<GlobalContactsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  GlobalContactsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<GlobalContactsMap> set(value, [object]) async {
    if (value is GlobalContactsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
