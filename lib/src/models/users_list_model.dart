import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class UsersList extends DbList<DbString> {    UsersList([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<UsersList> get future async =>
      await super.future as UsersList;
      
  @override
  Stream<UsersList> get stream => super.getStream<UsersList>();
  
  @override
  Stream<UsersList> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
   @override
  UsersList get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UsersList> set(value, [object]) async {
    if (value is UsersList) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
