import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Chat extends DbBaseType{
Chat([String key]) : super(key);

Header get header => get(Header('header'));
set header(value) => header.set(value);
DbDuration get timestamp => get(DbDuration('timestamp'));
set timestamp(value) => timestamp.set(value);
MessagesMap get messages => get(MessagesMap('messages', (key) => Message(key)));
set messages(value) => messages.set(value);
DbString get cid => get(DbString('cid'));
set cid(value) => cid.set(value);
UsersList get users => get(UsersList('users', (key) => DbString(key)));
set users(value) => users.set(value);
UsersInfoMap get usersInfo => get(UsersInfoMap('users_info', (key) => UserInformation(key)));
set usersInfo(value) => usersInfo.set(value);
@override

Future<Chat> get future async => await super.future as Chat;
@override

Stream<Chat> get stream => super.getStream<Chat>();
@override

Stream<Chat> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Chat get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Chat> set(value) async {
    if (value is Chat) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
