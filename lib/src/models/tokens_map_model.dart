import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class TokensMap extends DbMap<DbString, DbString> {    TokensMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<TokensMap> get future async =>
      await super.future as TokensMap;
      
  @override
  Stream<TokensMap> get stream => super.getStream<TokensMap>();
  
  @override
  Stream<TokensMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  TokensMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<TokensMap> set(value, [object]) async {
    if (value is TokensMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
