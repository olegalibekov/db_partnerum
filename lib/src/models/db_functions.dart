import '../../db_partnerum.dart';
/*abstract class DbFunctions{static void newChatCreation(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chat[keyID0].set(value);
}
static void deleteUselessImage(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.imagesContainer[keyID0].set(value);
}
static void changeRating(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.userData[keyID0].sumRating:Double[keyID1].set(value);
}
static void creatingUser(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.userData[keyID0].userPhone:String[keyID1].set(value);
}
static void onMessage(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chat[keyID0].message[keyID1].set(value);
}
static void usernameUpdate(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.userData[keyID0].userInformation[keyID1].set(value);
}
static void onAccountRemoved(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.userData[keyID0].userInformation[keyID1].set(value);
}
static void avatarUpdate(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.userData[keyID0].userInformation[keyID1].set(value);
}
static void changeHouse(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.userData[keyID0].houses[keyID1].set(value);
}
static void creatingChat(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].set(value);
}
static void newMessage(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chat[keyID0].message[keyID1].set(value);
}
static void newMessageTimestamp(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chat[keyID0].message[keyID1].set(value);
}
static void changeRequestInfo(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.userData[keyID0].requests[keyID1].requestInformation[keyID2].set(value);
}
static void offerObjectCreation(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].ownerUser[keyID2].set(value);
}
static void statusesHandler(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].usersStatuses[keyID2].set(value);
}
static void ownerRatingAndReview(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].ratingAndReview[keyID2].set(value);
}
static void requestorRatingAndReview(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].ratingAndReview[keyID2].set(value);
}
static void ownerReview(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].ownerReview:Review[keyID2].set(value);
}
static void requestorReview(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].requestUserReview:Review[keyID2].set(value);
}
static void requestorReviewAnswer(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].reviewAnswer[keyID2].set(value);
}
static void ownerReviewAnswer(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].reviewAnswer[keyID2].set(value);
}
static void changeRequest(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.userData[keyID0].requests[keyID1].requestInformation[keyID2].set(value);
}
static void requestorStatusTracker(Map jsonLog) async{
var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];
await db.root.chatTopic[keyID0].offerObject[keyID1].usersStatuses[keyID2].set(value);
}
}*/