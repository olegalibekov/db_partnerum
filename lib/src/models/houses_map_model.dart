import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class HousesMap extends DbMap<DbString, House> {    HousesMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<HousesMap> get future async =>
      await super.future as HousesMap;
      
  @override
  Stream<HousesMap> get stream => super.getStream<HousesMap>();
  
  @override
  Stream<HousesMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  HousesMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<HousesMap> set(value, [object]) async {
    if (value is HousesMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
