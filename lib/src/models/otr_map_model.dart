import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class OtrMap extends DbMap<DbString, Interlocutor> {    OtrMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<OtrMap> get future async =>
      await super.future as OtrMap;
      
  @override
  Stream<OtrMap> get stream => super.getStream<OtrMap>();
  
  @override
  Stream<OtrMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  OtrMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<OtrMap> set(value, [object]) async {
    if (value is OtrMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
