import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Header extends DbBaseType{
Header([String key]) : super(key);

DbString get type => get(DbString('type'));
set type(value) => type.set(value);
DbString get offerObjectId => get(DbString('offer_object_id'));
set offerObjectId(value) => offerObjectId.set(value);
@override

Future<Header> get future async => await super.future as Header;
@override

Stream<Header> get stream => super.getStream<Header>();
@override

Stream<Header> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Header get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Header> set(value) async {
    if (value is Header) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
