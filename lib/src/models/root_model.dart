import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Root extends DbBaseType{
Root([String key]) : super(key){
    LocalDatabaseManager.rootMaps[this] = {};
  }
    ///Есть вариант, когда можно получить инстанцию на обьект в любом случае.


DbNumber<int> get taskToAccomplish => get(DbNumber<int>('task_to_accomplish'));
set taskToAccomplish(value) => taskToAccomplish.set(value);
AdminsListMap get adminsList => get(AdminsListMap('admins_list', (key) => DbNumber<int>(key)));
set adminsList(value) => adminsList.set(value);
GlobalContactsMap get globalContacts => get(GlobalContactsMap('global_contacts', (key) => DbString(key)));
set globalContacts(value) => globalContacts.set(value);
UserDataMap get userData => get(UserDataMap('user_data', (key) => User(key)));
set userData(value) => userData.set(value);
DbString get availableCities => get(DbString('available_cities'));
set availableCities(value) => availableCities.set(value);
ChatTopic get chatTopic => get(ChatTopic('chat_topic'));
set chatTopic(value) => chatTopic.set(value);
Chat get chat => get(Chat('chat'));
set chat(value) => chat.set(value);
Condition get condition => get(Condition('condition'));
set condition(value) => condition.set(value);
ImagesContainerMap get imagesContainer => get(ImagesContainerMap('images_container', (key) => PhotoImage(key)));
set imagesContainer(value) => imagesContainer.set(value);
@override

Future<Root> get future async => await super.future as Root;
@override

Stream<Root> get stream => super.getStream<Root>();
@override

Stream<Root> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Root get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Root> set(value) async {
    if (value is Root) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
