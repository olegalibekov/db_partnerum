import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Position extends DbBaseType{
Position([String key]) : super(key);

DbString get latitude => get(DbString('latitude'));
set latitude(value) => latitude.set(value);
DbString get longitude => get(DbString('longitude'));
set longitude(value) => longitude.set(value);
@override

Future<Position> get future async => await super.future as Position;
@override

Stream<Position> get stream => super.getStream<Position>();
@override

Stream<Position> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Position get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Position> set(value) async {
    if (value is Position) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
