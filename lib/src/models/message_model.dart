import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Message extends DbBaseType{
Message([String key]) : super(key);

DbString get uid => get(DbString('uid'));
set uid(value) => uid.set(value);
DbDuration get timestamp => get(DbDuration('timestamp'));
set timestamp(value) => timestamp.set(value);
DbString get message => get(DbString('message'));
set message(value) => message.set(value);
ImagesMap get images => get(ImagesMap('images', (key) => DbObj(key)));
set images(value) => images.set(value);
DbBool get isEvent => get(DbBool('is_event'));
set isEvent(value) => isEvent.set(value);
@override

Future<Message> get future async => await super.future as Message;
@override

Stream<Message> get stream => super.getStream<Message>();
@override

Stream<Message> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Message get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Message> set(value) async {
    if (value is Message) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
