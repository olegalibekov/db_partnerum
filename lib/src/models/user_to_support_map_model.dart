import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class UserToSupportMap extends DbMap<DbString, Chat> {    UserToSupportMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<UserToSupportMap> get future async =>
      await super.future as UserToSupportMap;
      
  @override
  Stream<UserToSupportMap> get stream => super.getStream<UserToSupportMap>();
  
  @override
  Stream<UserToSupportMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  UserToSupportMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UserToSupportMap> set(value, [object]) async {
    if (value is UserToSupportMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
