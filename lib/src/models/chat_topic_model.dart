import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class ChatTopic extends DbBaseType{
ChatTopic([String key]) : super(key);

UserToUserMap get userToUser => get(UserToUserMap('user_to_user', (key) => Chat(key)));
set userToUser(value) => userToUser.set(value);
UserToSupportMap get userToSupport => get(UserToSupportMap('user_to_support', (key) => Chat(key)));
set userToSupport(value) => userToSupport.set(value);
Offer2RoomMap get offer2Room => get(Offer2RoomMap('offer2room', (key) => OfferObject(key)));
set offer2Room(value) => offer2Room.set(value);
@override

Future<ChatTopic> get future async => await super.future as ChatTopic;
@override

Stream<ChatTopic> get stream => super.getStream<ChatTopic>();
@override

Stream<ChatTopic> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  ChatTopic get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<ChatTopic> set(value) async {
    if (value is ChatTopic) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
