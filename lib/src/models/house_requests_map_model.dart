import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class HouseRequestsMap extends DbMap<DbString, HouseRequestInfo> {    HouseRequestsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<HouseRequestsMap> get future async =>
      await super.future as HouseRequestsMap;
      
  @override
  Stream<HouseRequestsMap> get stream => super.getStream<HouseRequestsMap>();
  
  @override
  Stream<HouseRequestsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  HouseRequestsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<HouseRequestsMap> set(value, [object]) async {
    if (value is HouseRequestsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
