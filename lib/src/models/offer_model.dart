import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Offer extends DbBaseType{
Offer([String key]) : super(key);

DbString get userId => get(DbString('user_id'));
set userId(value) => userId.set(value);
DbString get houseId => get(DbString('house_id'));
set houseId(value) => houseId.set(value);
DbString get offerId => get(DbString('offer_id'));
set offerId(value) => offerId.set(value);
@override

Future<Offer> get future async => await super.future as Offer;
@override

Stream<Offer> get stream => super.getStream<Offer>();
@override

Stream<Offer> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Offer get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Offer> set(value) async {
    if (value is Offer) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
