import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class UsersReview extends DbBaseType{
UsersReview([String key]) : super(key);

DbNumber<double> get mark => get(DbNumber<double>('mark'));
set mark(value) => mark.set(value);
DbString get reviewText => get(DbString('review_text'));
set reviewText(value) => reviewText.set(value);
DbString get answerText => get(DbString('answer_text'));
set answerText(value) => answerText.set(value);
@override

Future<UsersReview> get future async => await super.future as UsersReview;
@override

Stream<UsersReview> get stream => super.getStream<UsersReview>();
@override

Stream<UsersReview> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  UsersReview get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UsersReview> set(value) async {
    if (value is UsersReview) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
