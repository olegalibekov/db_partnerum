import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class ImagesContainerMap extends DbMap<DbString, PhotoImage> {    ImagesContainerMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<ImagesContainerMap> get future async =>
      await super.future as ImagesContainerMap;
      
  @override
  Stream<ImagesContainerMap> get stream => super.getStream<ImagesContainerMap>();
  
  @override
  Stream<ImagesContainerMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  ImagesContainerMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<ImagesContainerMap> set(value, [object]) async {
    if (value is ImagesContainerMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
