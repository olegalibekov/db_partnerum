import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class StationsList extends DbList<DbString> {    StationsList([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<StationsList> get future async =>
      await super.future as StationsList;
      
  @override
  Stream<StationsList> get stream => super.getStream<StationsList>();
  
  @override
  Stream<StationsList> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
   @override
  StationsList get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<StationsList> set(value, [object]) async {
    if (value is StationsList) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
