import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class RequestInformation extends DbBaseType{
RequestInformation([String key]) : super(key);

DbString get city => get(DbString('city'));
set city(value) => city.set(value);
DbNumber<int> get commision => get(DbNumber<int>('commision'));
set commision(value) => commision.set(value);
DatesRange get datesRange => get(DatesRange('dates_range'));
set datesRange(value) => datesRange.set(value);
DbString get description => get(DbString('description'));
set description(value) => description.set(value);
DbNumber<int> get guests => get(DbNumber<int>('guests'));
set guests(value) => guests.set(value);
Position get position => get(Position('position'));
set position(value) => position.set(value);
DbDuration get timeToMetro => get(DbDuration('time_to_metro'));
set timeToMetro(value) => timeToMetro.set(value);
PriceRange get priceRange => get(PriceRange('price_range'));
set priceRange(value) => priceRange.set(value);
DbString get rooms => get(DbString('rooms'));
set rooms(value) => rooms.set(value);
StationsMap get stations => get(StationsMap('stations', (key) => DbString(key)));
set stations(value) => stations.set(value);
DbBool get isArchived => get(DbBool('is_archived'));
set isArchived(value) => isArchived.set(value);
DbDuration get lastChange => get(DbDuration('last_change'));
set lastChange(value) => lastChange.set(value);
@override

Future<RequestInformation> get future async => await super.future as RequestInformation;
@override

Stream<RequestInformation> get stream => super.getStream<RequestInformation>();
@override

Stream<RequestInformation> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  RequestInformation get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<RequestInformation> set(value) async {
    if (value is RequestInformation) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
