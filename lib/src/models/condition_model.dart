import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Condition extends DbBaseType{
Condition([String key]) : super(key);

DbString get type => get(DbString('type'));
set type(value) => type.set(value);
DbDuration get timestamp => get(DbDuration('timestamp'));
set timestamp(value) => timestamp.set(value);
DbString get author => get(DbString('author'));
set author(value) => author.set(value);
DbString get lastAction => get(DbString('last_action'));
set lastAction(value) => lastAction.set(value);
@override

Future<Condition> get future async => await super.future as Condition;
@override

Stream<Condition> get stream => super.getStream<Condition>();
@override

Stream<Condition> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Condition get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Condition> set(value) async {
    if (value is Condition) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
