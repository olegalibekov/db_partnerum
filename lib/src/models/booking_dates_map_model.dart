import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class BookingDatesMap extends DbMap<DbString, BookingPeriod> {    BookingDatesMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<BookingDatesMap> get future async =>
      await super.future as BookingDatesMap;
      
  @override
  Stream<BookingDatesMap> get stream => super.getStream<BookingDatesMap>();
  
  @override
  Stream<BookingDatesMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  BookingDatesMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<BookingDatesMap> set(value, [object]) async {
    if (value is BookingDatesMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
