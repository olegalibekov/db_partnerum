import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class OffersMap extends DbMap<DbString, Offer> {    OffersMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<OffersMap> get future async =>
      await super.future as OffersMap;
      
  @override
  Stream<OffersMap> get stream => super.getStream<OffersMap>();
  
  @override
  Stream<OffersMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  OffersMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<OffersMap> set(value, [object]) async {
    if (value is OffersMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
