import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class UtuMap extends DbMap<DbString, Interlocutor> {    UtuMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<UtuMap> get future async =>
      await super.future as UtuMap;
      
  @override
  Stream<UtuMap> get stream => super.getStream<UtuMap>();
  
  @override
  Stream<UtuMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  UtuMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UtuMap> set(value, [object]) async {
    if (value is UtuMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
