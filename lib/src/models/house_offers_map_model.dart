import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class HouseOffersMap extends DbMap<DbString, DbString> {    HouseOffersMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<HouseOffersMap> get future async =>
      await super.future as HouseOffersMap;
      
  @override
  Stream<HouseOffersMap> get stream => super.getStream<HouseOffersMap>();
  
  @override
  Stream<HouseOffersMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  HouseOffersMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<HouseOffersMap> set(value, [object]) async {
    if (value is HouseOffersMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
