import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class HouseRequestInfo extends DbBaseType{
HouseRequestInfo([String key]) : super(key);

DbString get offerId => get(DbString('offer_id'));
set offerId(value) => offerId.set(value);
DbBool get isArchived => get(DbBool('is_archived'));
set isArchived(value) => isArchived.set(value);
DbString get requesterId => get(DbString('requester_id'));
set requesterId(value) => requesterId.set(value);
@override

Future<HouseRequestInfo> get future async => await super.future as HouseRequestInfo;
@override

Stream<HouseRequestInfo> get stream => super.getStream<HouseRequestInfo>();
@override

Stream<HouseRequestInfo> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  HouseRequestInfo get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<HouseRequestInfo> set(value) async {
    if (value is HouseRequestInfo) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
