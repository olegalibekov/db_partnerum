import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class UserChats extends DbBaseType{
UserChats([String key]) : super(key);

UtuMap get utu => get(UtuMap('utu', (key) => Interlocutor(key)));
set utu(value) => utu.set(value);
UtsMap get uts => get(UtsMap('uts', (key) => Interlocutor(key)));
set uts(value) => uts.set(value);
OtrMap get otr => get(OtrMap('otr', (key) => Interlocutor(key)));
set otr(value) => otr.set(value);
@override

Future<UserChats> get future async => await super.future as UserChats;
@override

Stream<UserChats> get stream => super.getStream<UserChats>();
@override

Stream<UserChats> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  UserChats get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UserChats> set(value) async {
    if (value is UserChats) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
