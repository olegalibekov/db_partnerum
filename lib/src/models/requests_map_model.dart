import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class RequestsMap extends DbMap<DbString, Request> {    RequestsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<RequestsMap> get future async =>
      await super.future as RequestsMap;
      
  @override
  Stream<RequestsMap> get stream => super.getStream<RequestsMap>();
  
  @override
  Stream<RequestsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  RequestsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<RequestsMap> set(value, [object]) async {
    if (value is RequestsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
