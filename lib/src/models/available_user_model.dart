import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class AvailableUser extends DbBaseType{
AvailableUser([String key]) : super(key);

UserInformation get userInformation => get(UserInformation('user_information'));
set userInformation(value) => userInformation.set(value);
DbString get chatId => get(DbString('chat_id'));
set chatId(value) => chatId.set(value);
@override

Future<AvailableUser> get future async => await super.future as AvailableUser;
@override

Stream<AvailableUser> get stream => super.getStream<AvailableUser>();
@override

Stream<AvailableUser> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  AvailableUser get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<AvailableUser> set(value) async {
    if (value is AvailableUser) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
