import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class ImagesMap extends DbMap<DbString, DbObj> {    ImagesMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<ImagesMap> get future async =>
      await super.future as ImagesMap;
      
  @override
  Stream<ImagesMap> get stream => super.getStream<ImagesMap>();
  
  @override
  Stream<ImagesMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  ImagesMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<ImagesMap> set(value, [object]) async {
    if (value is ImagesMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
