import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class UserDataMap extends DbMap<DbString, User> {    UserDataMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<UserDataMap> get future async =>
      await super.future as UserDataMap;
      
  @override
  Stream<UserDataMap> get stream => super.getStream<UserDataMap>();
  
  @override
  Stream<UserDataMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  UserDataMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UserDataMap> set(value, [object]) async {
    if (value is UserDataMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
