import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class PhotoImage extends DbBaseType{
PhotoImage([String key]) : super(key);

DbObj get bigImg => get(DbObj('big_img'));
set bigImg(value) => bigImg.set(value);
DbObj get smallImg => get(DbObj('small_img'));
set smallImg(value) => smallImg.set(value);
DbNumber<int> get usagePlacesAmount => get(DbNumber<int>('usage_places_amount'));
set usagePlacesAmount(value) => usagePlacesAmount.set(value);
@override

Future<PhotoImage> get future async => await super.future as PhotoImage;
@override

Stream<PhotoImage> get stream => super.getStream<PhotoImage>();
@override

Stream<PhotoImage> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  PhotoImage get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<PhotoImage> set(value) async {
    if (value is PhotoImage) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
