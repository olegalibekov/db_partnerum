import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class Offer2RoomMap extends DbMap<DbString, OfferObject> {    Offer2RoomMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<Offer2RoomMap> get future async =>
      await super.future as Offer2RoomMap;
      
  @override
  Stream<Offer2RoomMap> get stream => super.getStream<Offer2RoomMap>();
  
  @override
  Stream<Offer2RoomMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  Offer2RoomMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Offer2RoomMap> set(value, [object]) async {
    if (value is Offer2RoomMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
