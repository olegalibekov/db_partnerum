import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class UnreadChatsMap extends DbMap<DbString, DbString> {    UnreadChatsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<UnreadChatsMap> get future async =>
      await super.future as UnreadChatsMap;
      
  @override
  Stream<UnreadChatsMap> get stream => super.getStream<UnreadChatsMap>();
  
  @override
  Stream<UnreadChatsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  UnreadChatsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UnreadChatsMap> set(value, [object]) async {
    if (value is UnreadChatsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
