import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class BookingPeriod extends DbBaseType{
BookingPeriod([String key]) : super(key);

DbDuration get checkIn => get(DbDuration('check_in'));
set checkIn(value) => checkIn.set(value);
DbDuration get checkOut => get(DbDuration('check_out'));
set checkOut(value) => checkOut.set(value);
@override

Future<BookingPeriod> get future async => await super.future as BookingPeriod;
@override

Stream<BookingPeriod> get stream => super.getStream<BookingPeriod>();
@override

Stream<BookingPeriod> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  BookingPeriod get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<BookingPeriod> set(value) async {
    if (value is BookingPeriod) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
