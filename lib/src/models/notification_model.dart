import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Notification extends DbBaseType{
Notification([String key]) : super(key);

DbString get title => get(DbString('title'));
set title(value) => title.set(value);
DbString get image => get(DbString('image'));
set image(value) => image.set(value);
DbString get body => get(DbString('body'));
set body(value) => body.set(value);
@override

Future<Notification> get future async => await super.future as Notification;
@override

Stream<Notification> get stream => super.getStream<Notification>();
@override

Stream<Notification> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Notification get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Notification> set(value) async {
    if (value is Notification) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
