import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class User extends DbBaseType{
User([String key]) : super(key);

NotificationsMap get notifications => get(NotificationsMap('notifications', (key) => UserNotification(key)));
set notifications(value) => notifications.set(value);
RequestsMap get requests => get(RequestsMap('requests', (key) => Request(key)));
set requests(value) => requests.set(value);
UserInformation get userInformation => get(UserInformation('user_information'));
set userInformation(value) => userInformation.set(value);
HousesMap get houses => get(HousesMap('houses', (key) => House(key)));
set houses(value) => houses.set(value);
UserChats get userChats => get(UserChats('user_chats'));
set userChats(value) => userChats.set(value);
AvailableUsersMap get availableUsers => get(AvailableUsersMap('available_users', (key) => AvailableUser(key)));
set availableUsers(value) => availableUsers.set(value);
FcmList get fcm => get(FcmList('fcm', (key) => DbString(key)));
set fcm(value) => fcm.set(value);
ConditionsMap get conditions => get(ConditionsMap('conditions', (key) => Condition(key)));
set conditions(value) => conditions.set(value);
ReviewsMap get reviews => get(ReviewsMap('reviews', (key) => UsersReview(key)));
set reviews(value) => reviews.set(value);
DbNumber<double> get sumRating => get(DbNumber<double>('sum_rating'));
set sumRating(value) => sumRating.set(value);
Document get document => get(Document('document'));
set document(value) => document.set(value);
ActiveChatsMap get activeChats => get(ActiveChatsMap('active_chats', (key) => DbBool(key)));
set activeChats(value) => activeChats.set(value);
UnreadChatsMap get unreadChats => get(UnreadChatsMap('unread_chats', (key) => DbString(key)));
set unreadChats(value) => unreadChats.set(value);
DbNumber<double> get reviewsNumber => get(DbNumber<double>('reviews_number'));
set reviewsNumber(value) => reviewsNumber.set(value);
DbBool get apartmentsAutoBookingDates => get(DbBool('apartments_auto_booking_dates'));
set apartmentsAutoBookingDates(value) => apartmentsAutoBookingDates.set(value);
DbString get userPhone => get(DbString('user_phone'));
set userPhone(value) => userPhone.set(value);
@override

Future<User> get future async => await super.future as User;
@override

Stream<User> get stream => super.getStream<User>();
@override

Stream<User> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  User get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<User> set(value) async {
    if (value is User) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
