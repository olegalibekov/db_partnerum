import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class FcmList extends DbList<DbString> {    FcmList([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<FcmList> get future async =>
      await super.future as FcmList;
      
  @override
  Stream<FcmList> get stream => super.getStream<FcmList>();
  
  @override
  Stream<FcmList> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
   @override
  FcmList get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<FcmList> set(value, [object]) async {
    if (value is FcmList) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
