import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class HouseRequest extends DbBaseType{
HouseRequest([String key]) : super(key);

DbString get string => get(DbString('string'));
set string(value) => string.set(value);
DbBool get isArchived => get(DbBool('is_archived'));
set isArchived(value) => isArchived.set(value);
@override

Future<HouseRequest> get future async => await super.future as HouseRequest;
@override

Stream<HouseRequest> get stream => super.getStream<HouseRequest>();
@override

Stream<HouseRequest> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  HouseRequest get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<HouseRequest> set(value) async {
    if (value is HouseRequest) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
