import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class PriceRange extends DbBaseType{
PriceRange([String key]) : super(key);

DbString get startPosition => get(DbString('start_position'));
set startPosition(value) => startPosition.set(value);
DbString get endPosition => get(DbString('end_position'));
set endPosition(value) => endPosition.set(value);
@override

Future<PriceRange> get future async => await super.future as PriceRange;
@override

Stream<PriceRange> get stream => super.getStream<PriceRange>();
@override

Stream<PriceRange> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  PriceRange get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<PriceRange> set(value) async {
    if (value is PriceRange) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
