import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class ConditionsMap extends DbMap<DbString, Condition> {    ConditionsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<ConditionsMap> get future async =>
      await super.future as ConditionsMap;
      
  @override
  Stream<ConditionsMap> get stream => super.getStream<ConditionsMap>();
  
  @override
  Stream<ConditionsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  ConditionsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<ConditionsMap> set(value, [object]) async {
    if (value is ConditionsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
