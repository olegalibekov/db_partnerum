import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class ActiveChatsMap extends DbMap<DbString, DbBool> {    ActiveChatsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<ActiveChatsMap> get future async =>
      await super.future as ActiveChatsMap;
      
  @override
  Stream<ActiveChatsMap> get stream => super.getStream<ActiveChatsMap>();
  
  @override
  Stream<ActiveChatsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  ActiveChatsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<ActiveChatsMap> set(value, [object]) async {
    if (value is ActiveChatsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
