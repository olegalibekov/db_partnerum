import 'package:db_partnerum/src/manager/src/g_storage_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';


import 'dart:typed_data';

import 'package:path/path.dart';

class FileWrapper {
  final String fileName;
  final Uint8List bytes;

  FileWrapper(this.fileName, this.bytes);
}

class DbObj extends DbString {
  DbObj(String key) : super(key);

  FileWrapper fileWrapper;

  ///[value] в данном случае это просто название файла, который хранится внутри.
  @override
  String get value => super.value?.toString() ?? null;

  @override
  Future<DbObj> get future async {
    if (value == null) {
      await super.future;
      if (value == null) {
        print('obj is empty. maybe you ve deleted file completely?');
      }
    } else if (value.toString() == '0') {
      print('looks like your object hasnt loaded completely. try onObjLoaded');
      return this;
    }
    var filename = basename(Uri.parse(value).pathSegments.last);
    fileWrapper = FileWrapper(
        '$filename', await GStorageHelper.fileBytes(_path(), value));
    return this;
  }

  ///[metadataFuture] - загрузка служебной информации.
  Future<DbObj> get metadataFuture async {
    await super.future;
    return this;
  }

  ///when obj after future is still empty
  onObjLoaded() => stream.firstWhere((DbString element) =>
  element.value != null && element.value.toString() != '0');

  ///
  bool autoValue = false;

  ///От переменной [copy2base] зависит,
  ///загрузит ли полную копию база или же она будет возвращать ссылку.
  bool copy2base = false;

  ///Используя данную функцию, вы можете записать как url,
  @override
  void set value(value) => set(value);

  ///ссылка для загрузки файла
  String get url => GStorageHelper.url(value, _path());

  ///ссылка для загрузки файла со специальным токеном
  Future<String> get urlWithToken =>
      GStorageHelper.urlWithToken(value, _path());

  ///хеадер для получения доступа к файлу.
  Map<String, String> get syncHttpHeaders => GStorageHelper.syncHttpHeaders;

  bool uploadComplex = false;

  @override
  Future<DbBaseType> set(value) async {
    String fileName;
    if (this.value == null) {
      await super.future;
    }
    if (this.value != null) {
      await _objRemove();
    }
    if (value is FileWrapper) {
      if (this.value == null) {
        await super.set('0');
      }
      fileWrapper = value;

      fileName = await GStorageHelper.uploadFile(value, _path());
    } else if (value is String) {
      fileName = value;
    }
    var completedSet = await super.set(fileName);
    uploadComplex = false;
    return completedSet;
  }

  @override
  remove([Object key]) async {
    await _objRemove();
    await super.remove();
    fileWrapper = null;
  }

  _objRemove() async {
    if (value != null) {
      var uri = Uri.tryParse(value);
      if (!uri.isAbsolute) {
        await GStorageHelper.delete(value, _path());
      }
    }
  }

  String _path() {
    var stringifiedPath = DbUtils.stringifyPath(dbPath).join('/');
    if (stringifiedPath[0] == '/') {
      stringifiedPath = stringifiedPath.replaceFirst('/', '');
    }
    return stringifiedPath;
  }

  Uint8List get uint8list {
    if (fileWrapper == null) {
      return null;
    }
    return fileWrapper.bytes;
  }

  @override
  Stream<DbObj> get stream => super.getStream<DbString>();

  @override
  Stream<DbObj> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
}

class DbObjCache {
  String url;
  Uint8List data;

  DbObjCache(this.url, this.data);
}
