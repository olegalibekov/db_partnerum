
import 'package:db_partnerum/src/manager/src/settings_manager.dart';
import 'package:db_partnerum/src/manager/src/database_events.dart';
import '../../../db_partnerum.dart';
import 'dart:math';

import 'package:event_container/event_container.dart';

import 'types.dart';

abstract class DbMap<K extends DbBaseType, V extends DbBaseType>
    implements Map, DbBaseType {
  DbMap(this.key, this.objectCreator, [this.before]) {
    if (key == null) {
      settings.packet.on();
    }
  }

  DbBaseType before;

  @override
  String toString() => '${runtimeType}:${key.toString()}';

  call() {
    throw "you can't do this with map";
  }

  ///Дополнительный локальный список, который хранит внутри себя список активных операций.
  ///Он позволит дожидаться выполнения актуальных задач.
  var operationsList = <EventContainer<DbEvent, DbSetPair>>[];

  waitFor(DbEvent lastOperationType) {
    var lastOperation =
    operationsList.lastWhere((task) => task.eventType == lastOperationType);
    return DbUtils.taskCompleteWaiter(lastOperation);
  }

  ///TODO
  bool get isSync => value != null && databaseManager.pathTasks(this).isEmpty;

  @override
  String get etag {
    if (DbUtils.isDbBaseTypeSimple(this)) {
      DbEtag etag = get(DbEtag());
      return etag?.value ?? '';
    }
    return '';
  }

  @override
  String key;

  static Map<int, DbBaseType> pushMapInd = {};

  V get push {
    var uniqueId;
    while (uniqueId == null) {
      var tmpUniqueId = Random().nextInt(100000);
      if (!pushMapInd.containsKey(tmpUniqueId)) {
        uniqueId = tmpUniqueId;
      }
    }
    var pushM = this['_push$uniqueId'];
    pushMapInd[uniqueId] = pushM;
    return pushM;
  }

  Function(String key) objectCreator;

  @override
  V operator [](Object key) {
    ///TODO keywords
    var object = objectCreator(key) as DbBaseType;
    return get(object);
  }

  @override
  void operator []=(key, value) => this[key].set(value);

  @override
  void addAll(Map other) {
    // TODO: implement addAll
  }

  @override
  void addEntries(Iterable<MapEntry> newEntries) {
    // TODO: implement addEntries
  }

  @override
  Map<RK, RV> cast<RK, RV>() {
    // TODO: implement cast
    throw UnimplementedError();
  }

  @override
  Future<void> clear() async {
    var task =
    EventContainer<DbEvent, DbSetPair>(DbEvent.remove, DbSetPair(this));
    databaseManager.operationsFlow.add(task);
    await DbUtils.operationsListImplementer(operationsList, task);
  }

  @override
  bool containsKey(Object key) => representation.containsKey(key);

  @override
  bool containsValue(Object value) => representation.containsValue(value);

  @override
  Iterable<MapEntry> get entries => representation.entries.map((e) => null);

  @override
  void forEach(void Function(K key, V value) f) {
    // TODO: implement forEach
  }

  @override
  bool get isEmpty => representation?.isEmpty ?? true;

  get representation => databaseManager.localDatabaseManager[this];

  @override
  bool get isNotEmpty => representation?.isNotEmpty ?? false;

  @override
  Iterable get keys => representation.keys;

  @override
  int get length => representation.length;

  @override
  Map<K2, V2> map<K2, V2>(MapEntry<K2, V2> Function(K key, V value) f) {
    // TODO: implement map
    throw UnimplementedError();
  }

  @override
  putIfAbsent(key, Function() ifAbsent) {
    // TODO: implement putIfAbsent
    throw UnimplementedError();
  }

  @override
  remove([Object key]) async {
    if (key == null) throw "in map use instead of remove -> clear";
    var task =
    EventContainer<DbEvent, DbSetPair>(DbEvent.remove, DbSetPair(this));
    databaseManager.operationsFlow.add(task);
    await DbUtils.operationsListImplementer(operationsList, task);
  }

  @override
  void removeWhere(bool Function(K key, V value) predicate) {
    // TODO: implement removeWhere
  }

  @override
  update(key, Function(V value) update, {Function() ifAbsent}) {
    // TODO: implement update
    throw UnimplementedError();
  }

  @override
  void updateAll(Function(K key, V value) update) {
    // TODO: implement updateAll
  }

  @override
  Iterable<V> get values {
    if (representation is! Map) {
      return (representation as List)?.map<V>((e) => this[e]);
    }
    var keys = representation?.keys as Iterable;
    return keys?.where((key) => key != RealtimeDatabaseManager.Etag)?.map<V>((e) => this[e]);
  }

  @override
  get future async {
    if (settings.lock.isOn()) return this;
    var task = EventContainer<DbEvent, DbSetPair>(DbEvent.get, DbSetPair(this));
    databaseManager.operationsFlow.add(task);
    await DbUtils.operationsListImplementer(operationsList, task);
    return this;
  }

  @override
  get local => throw UnimplementedError();

  get stream;

  Stream get futureBasedStream;

  @override
  List<DbBaseType> get dbPath => DbUtils.before2dbPath(this);

  @override
  var selfRam;

  @override
  set(value, [object]) async {
    if (value is Future) value = await value;
    if (value is DbBaseType &&
        !(value.dbPath.first is Root) &&
        dbPath.length >= 2) {
      dbPath.reversed.elementAt(1).underGround[key] = value;
      value.key = key;
      value.before = before;
      databaseManager.localDatabaseManager.syncIt(value);
      /*
      return;

       */
    } else {
      var task =
      EventContainer<DbEvent, DbSetPair>(DbEvent.set, DbSetPair(this));
      databaseManager.operationsFlow.add(task);
      await DbUtils.operationsListImplementer(operationsList, task);
    }
  }

  @override
  Map<dynamic, dynamic> get value => selfRam;

  @override
  Future<void> upload() async {
    var task =
    EventContainer<DbEvent, DbSetPair>(DbEvent.upload, DbSetPair(this));
    databaseManager.operationsFlow.add(task);
    await DbUtils.operationsListImplementer(operationsList, task);
  }

  @override
  void set value(value) {
    // TODO: implement value
  }

  DbBaseTypeSettings _settings;

  DbBaseTypeSettings get settings {
    return _settings ??= DbBaseTypeSettings();
  }

  ///Задача данного [underGround] - хранение, текущих объектов
  Map underGround = {};

  @override
  get(object) {
    var tmp = underGround.putIfAbsent(object.key, () => object) as DbBaseType;
    if (tmp.before == null) tmp.before = this;
    return tmp;
  }

  Stream<T> getStream<T extends DbBaseType>() =>
      databaseManager.stream<T>(dbPath, this);

  ///Параметр [stepByStep] позволяет настроить интерактивность загрузки данных.
  bool stepByStep = false;
}
