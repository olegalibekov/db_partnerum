import 'package:db_partnerum/db_partnerum.dart';


class DbDuration extends DbBaseType<int> {
  DbDuration(String key) : super(key);

  @override
  Duration get value {
    var val = super.value;
    if (val != null && val is num) {
      return Duration(milliseconds: val);
    }
    return Duration.zero;
  }

  @override
  Stream get stream => super.getStream<DbDuration>();

  @override
  Stream<DbDuration> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);

  Future updateTimestamp() {
    return this.set(Keywords.timestamp);
  }
}

class DbDateTime extends DbBaseType<int> {
  DbDateTime(String key) : super(key);

  @override
  DateTime get value {
    var val = super.value;
    if (val != null && val is num) {
      return DateTime.fromMillisecondsSinceEpoch(val);
    }
    return DateTime.now();
  }

  @override
  Stream get stream => super.getStream<DbDateTime>();

  @override
  Stream<DbDateTime> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);

  Future updateTimestamp() {
    return this.set(Keywords.timestamp);
  }
}
