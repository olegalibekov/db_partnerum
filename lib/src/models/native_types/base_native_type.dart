
import 'package:db_partnerum/src/models/native_types/etag_type.dart';
import 'package:db_partnerum/src/manager/manager.dart';
import 'package:db_partnerum/src/manager/src/settings_manager.dart';
import 'package:db_partnerum/src/manager/src/database_events.dart';
import '../../../db_partnerum.dart';
import 'package:async/async.dart';
import 'package:event_container/event_container.dart';

Stopwatch stopwatch = Stopwatch();

abstract class DbBaseType<T> {
  var selfRam = null;

  ///Задача данного [underGround] - хранение, текущих объектов
  Map underGround = {};

  List<DbBaseType> get dbPath => DbUtils.before2dbPath(this);

  String key;

  String get etag {
    if (DbUtils.isDbBaseTypeSimple(this)) {
      DbEtag etag = get(DbEtag());
      return etag?.value ?? '';
    }
    return '';
  }

  DbBaseType before;

  DbBaseType(this.key, [this.before]) {
    if (key == null) {
      settings.packet.on();
    }
  }

  DbBaseType.zero();

  @override
  String toString() => '${runtimeType}:${key.toString()}';

  set value(value) => set(value);

  //LocalDatabaseManager.tmpValues[this] = value;
  get value => databaseManager.get(this);

  get local {}

  ///[stream] позволяет загружать данные напрямую с базы данных
  Stream get stream;

  ///[futureBasedStream] позволяет загрузить сначала данные из локального хранилища, а после отобразить данные из [future]
  Stream get futureBasedStream;



  DbBaseTypeSettings _settings;

  DbBaseTypeSettings get settings {
    return _settings ??= DbBaseTypeSettings();
  }

  ///TODO сделать более подробную переменную о происходящем с путем.
  ///Например - что в процессе загрузки. или уже полностью синхронизирован.
  ///Должна быть глобальная переменная для запуска + сохранение задач в локал, т.к. при выключении все потеряется.
  bool get isSync => value != null && databaseManager.pathTasks(this).isEmpty;

  //надо сделать установку большого папочки.
  get(DbBaseType object) {
    //var key = DbSessionKey.key(rawKey);
    var tmp = underGround.putIfAbsent(object.key, () => object) as DbBaseType;
    if (tmp.before == null) tmp.before = this;
    return tmp;
  }

  ///Дополнительный локальный список, который хранит внутри себя список активных операций.
  ///Он позволит дожидаться выполнения актуальных задач.
  var operationsList = <EventContainer<DbEvent, DbSetPair>>[];

  waitFor(DbEvent lastOperationType) {
    var lastOperation =
    operationsList.lastWhere((task) => task.eventType == lastOperationType);
    return DbUtils.taskCompleteWaiter(lastOperation);
  }

  call() => value;

  remove([Object key]) async {
    var task =
    EventContainer<DbEvent, DbSetPair>(DbEvent.remove, DbSetPair(this));
    databaseManager.operationsFlow.add(task);
    await DbUtils.operationsListImplementer(operationsList, task);
  }

  Future<void> upload() async {
    var task =
    EventContainer<DbEvent, DbSetPair>(DbEvent.upload, DbSetPair(this));
    databaseManager.operationsFlow.add(task);
    await DbUtils.operationsListImplementer(operationsList, task);
  }

  Future<DbBaseType> set(value) async {
    ///В данном случае функция занимается проверкой обьекта, а не установкой значения
    if (value is Future) value = await value;
    if (value is Keywords) {
      if (value == Keywords.timestamp) {
        value = value.toString();
      } else
        value = (value as Keywords).short;
    }

    ///is first ROOT?
    if (value is DbBaseType &&
        !(value.dbPath.first is Root) &&
        dbPath.length >= 2) {
      dbPath.reversed.elementAt(1).underGround[key] = value;
      value.key = key;
      value.before = before;
      databaseManager.localDatabaseManager.syncIt(value);
      return this;
    }

    var task =
    EventContainer<DbEvent, DbSetPair>(DbEvent.set, DbSetPair(this, value));
    databaseManager.operationsFlow.add(task);
    await DbUtils.operationsListImplementer(operationsList, task);
    return this;
  }

  get future async {
    if (settings.lock.isOn()) return this;
    var task = EventContainer<DbEvent, DbSetPair>(DbEvent.get, DbSetPair(this));
    databaseManager.operationsFlow.add(task);
    await DbUtils.operationsListImplementer(operationsList, task);
    return this;
  }

  Stream<T> getStream<T extends DbBaseType>() {
    return databaseManager.stream<T>(dbPath, this);
  }
}
