import 'package:db_partnerum/db_partnerum.dart';
class DbEtag extends DbString {
  DbEtag() : super(RealtimeDatabaseManager.Etag);
  @override
  Stream<DbEtag> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
}
