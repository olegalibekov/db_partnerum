
import 'package:db_partnerum/src/manager/src/database_events.dart';import '../../../db_partnerum.dart';
import 'types.dart';

// extension DbStr on String {
//   get dbStr {}
// }
class DbString extends DbBaseType<String> {
  DbString(String key) : super(key);

  @override
  Future<DbString> get future async => await super.future as DbString;

  @override
  String get value => super.value?.toString() ?? null;

  @override
  Stream get stream => super.getStream<DbString>();

  @override
  Stream<DbString> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);

  @override
  operator +(value2) async {
    var self = await future;
    return '${self.value}$value2';
  }

// set value(String value) {}
// String get value{
//
// }
}
