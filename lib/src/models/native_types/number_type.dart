
import 'package:db_partnerum/src/manager/src/database_events.dart';import '../../../db_partnerum.dart';
import 'types.dart';

// extension DbNum on num {
//   get dbNum {
//
//   }
// }
//

class DbNumber<T extends num> extends DbBaseType<T> {
  DbNumber(String key) : super(key);

  @override
  Future<DbNumber<T>> get future async => await super.future as DbNumber<T>;

  @override
  Stream get stream => super.getStream<DbNumber<T>>();

  @override
  Stream<DbNumber<T>> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);

  @override
  T get value => super.value;

  @override
  operator +(value2) async {
    var self = await future;
    return self.value + value2;
  }
// set value(T value) {}
// T get value{
//
// }
}
