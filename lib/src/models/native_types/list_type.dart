import 'package:db_partnerum/db_partnerum.dart';
class DbList<T extends DbBaseType> extends DbMap<DbNumber<int>, T> {
  DbList(String key, Function(String key) objectCreator)
      : super(key, objectCreator);

  @override
  Stream get stream => super.getStream<DbList<T>>();

  @override
  Stream<DbList<T>> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);

  List get list => super.value.values;

  List get representation => databaseManager.localDatabaseManager[this] as List;
}
