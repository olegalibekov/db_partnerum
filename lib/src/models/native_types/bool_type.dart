import 'package:db_partnerum/db_partnerum.dart';

import 'types.dart';

class DbBool extends DbBaseType {
  DbBool(String key) : super(key);

  @override
  Stream get stream => super.getStream<DbBool>();


  @override
  bool get value => super.value;

  @override
  Stream<DbBool> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
}
