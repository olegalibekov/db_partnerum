import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class AdminsListMap extends DbMap<DbString, DbNumber<int>> {    AdminsListMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<AdminsListMap> get future async =>
      await super.future as AdminsListMap;
      
  @override
  Stream<AdminsListMap> get stream => super.getStream<AdminsListMap>();
  
  @override
  Stream<AdminsListMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  AdminsListMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<AdminsListMap> set(value, [object]) async {
    if (value is AdminsListMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
