import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Document extends DbBaseType{
Document([String key]) : super(key);

DbObj get photo => get(DbObj('photo'));
set photo(value) => photo.set(value);
DbString get status => get(DbString('status'));
set status(value) => status.set(value);
DbDuration get timestamp => get(DbDuration('timestamp'));
set timestamp(value) => timestamp.set(value);
@override

Future<Document> get future async => await super.future as Document;
@override

Stream<Document> get stream => super.getStream<Document>();
@override

Stream<Document> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Document get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Document> set(value) async {
    if (value is Document) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
