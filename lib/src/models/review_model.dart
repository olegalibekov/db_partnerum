import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class Review extends DbBaseType{
Review([String key]) : super(key);

DbString get ratingAndReview => get(DbString('rating_and_review'));
set ratingAndReview(value) => ratingAndReview.set(value);
Review get ownerReview => get(Review('owner_review'));
set ownerReview(value) => ownerReview.set(value);
Review get requestUserReview => get(Review('request_user_review'));
set requestUserReview(value) => requestUserReview.set(value);
DbDuration get timestamp => get(DbDuration('timestamp'));
set timestamp(value) => timestamp.set(value);
DbString get reviewAnswer => get(DbString('review_answer'));
set reviewAnswer(value) => reviewAnswer.set(value);
DbDuration get reviewAnswerTimestamp => get(DbDuration('review_answer_timestamp'));
set reviewAnswerTimestamp(value) => reviewAnswerTimestamp.set(value);
DbString get authorId => get(DbString('author_id'));
set authorId(value) => authorId.set(value);
@override

Future<Review> get future async => await super.future as Review;
@override

Stream<Review> get stream => super.getStream<Review>();
@override

Stream<Review> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  Review get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<Review> set(value) async {
    if (value is Review) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
