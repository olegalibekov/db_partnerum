import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class UserToUserMap extends DbMap<DbString, Chat> {    UserToUserMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<UserToUserMap> get future async =>
      await super.future as UserToUserMap;
      
  @override
  Stream<UserToUserMap> get stream => super.getStream<UserToUserMap>();
  
  @override
  Stream<UserToUserMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  UserToUserMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UserToUserMap> set(value, [object]) async {
    if (value is UserToUserMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
