import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class House extends DbBaseType{
House([String key]) : super(key);

DbDuration get timestamp => get(DbDuration('timestamp'));
set timestamp(value) => timestamp.set(value);
HouseInformation get houseInformation => get(HouseInformation('house_information'));
set houseInformation(value) => houseInformation.set(value);
HouseRequestsMap get houseRequests => get(HouseRequestsMap('house_requests', (key) => HouseRequestInfo(key)));
set houseRequests(value) => houseRequests.set(value);
BookingDatesMap get bookingDates => get(BookingDatesMap('booking_dates', (key) => BookingPeriod(key)));
set bookingDates(value) => bookingDates.set(value);
@override

Future<House> get future async => await super.future as House;
@override

Stream<House> get stream => super.getStream<House>();
@override

Stream<House> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  House get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<House> set(value) async {
    if (value is House) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
