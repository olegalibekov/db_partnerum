import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class AvailableUsersMap extends DbMap<DbString, AvailableUser> {    AvailableUsersMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<AvailableUsersMap> get future async =>
      await super.future as AvailableUsersMap;
      
  @override
  Stream<AvailableUsersMap> get stream => super.getStream<AvailableUsersMap>();
  
  @override
  Stream<AvailableUsersMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  AvailableUsersMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<AvailableUsersMap> set(value, [object]) async {
    if (value is AvailableUsersMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
