import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class UserNotification extends DbBaseType{
UserNotification([String key]) : super(key);

DbBool get read => get(DbBool('read'));
set read(value) => read.set(value);
DbDuration get timestamp => get(DbDuration('timestamp'));
set timestamp(value) => timestamp.set(value);
Notification get notification => get(Notification('notification'));
set notification(value) => notification.set(value);
DbString get offerObjectId => get(DbString('offer_object_id'));
set offerObjectId(value) => offerObjectId.set(value);
DbString get chatId => get(DbString('chat_id'));
set chatId(value) => chatId.set(value);
@override

Future<UserNotification> get future async => await super.future as UserNotification;
@override

Stream<UserNotification> get stream => super.getStream<UserNotification>();
@override

Stream<UserNotification> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  UserNotification get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UserNotification> set(value) async {
    if (value is UserNotification) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
