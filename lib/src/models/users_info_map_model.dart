import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class UsersInfoMap extends DbMap<DbString, UserInformation> {    UsersInfoMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<UsersInfoMap> get future async =>
      await super.future as UsersInfoMap;
      
  @override
  Stream<UsersInfoMap> get stream => super.getStream<UsersInfoMap>();
  
  @override
  Stream<UsersInfoMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  UsersInfoMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UsersInfoMap> set(value, [object]) async {
    if (value is UsersInfoMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
