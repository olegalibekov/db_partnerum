import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class StationsMap extends DbMap<DbString, DbString> {    StationsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<StationsMap> get future async =>
      await super.future as StationsMap;
      
  @override
  Stream<StationsMap> get stream => super.getStream<StationsMap>();
  
  @override
  Stream<StationsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  StationsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<StationsMap> set(value, [object]) async {
    if (value is StationsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
