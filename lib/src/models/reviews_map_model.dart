import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class ReviewsMap extends DbMap<DbString, UsersReview> {    ReviewsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<ReviewsMap> get future async =>
      await super.future as ReviewsMap;
      
  @override
  Stream<ReviewsMap> get stream => super.getStream<ReviewsMap>();
  
  @override
  Stream<ReviewsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  ReviewsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<ReviewsMap> set(value, [object]) async {
    if (value is ReviewsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
