import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class UtsMap extends DbMap<DbString, Interlocutor> {    UtsMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<UtsMap> get future async =>
      await super.future as UtsMap;
      
  @override
  Stream<UtsMap> get stream => super.getStream<UtsMap>();
  
  @override
  Stream<UtsMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  UtsMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UtsMap> set(value, [object]) async {
    if (value is UtsMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
