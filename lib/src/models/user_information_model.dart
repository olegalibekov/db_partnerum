import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class UserInformation extends DbBaseType{
UserInformation([String key]) : super(key);

DbDuration get lastOnline => get(DbDuration('last_online'));
set lastOnline(value) => lastOnline.set(value);
DbObj get avatar => get(DbObj('avatar'));
set avatar(value) => avatar.set(value);
DbString get username => get(DbString('username'));
set username(value) => username.set(value);
DbString get document => get(DbString('document'));
set document(value) => document.set(value);
DbBool get isConfirmed => get(DbBool('is_confirmed'));
set isConfirmed(value) => isConfirmed.set(value);
DbBool get isAdmin => get(DbBool('is_admin'));
set isAdmin(value) => isAdmin.set(value);
DbString get role => get(DbString('role'));
set role(value) => role.set(value);
DbNumber<double> get rating => get(DbNumber<double>('rating'));
set rating(value) => rating.set(value);
DbString get uid => get(DbString('uid'));
set uid(value) => uid.set(value);
TokensMap get tokens => get(TokensMap('tokens', (key) => DbString(key)));
set tokens(value) => tokens.set(value);
DbBool get isRemoved => get(DbBool('is_removed'));
set isRemoved(value) => isRemoved.set(value);
DbString get avatarId => get(DbString('avatar_id'));
set avatarId(value) => avatarId.set(value);
@override

Future<UserInformation> get future async => await super.future as UserInformation;
@override

Stream<UserInformation> get stream => super.getStream<UserInformation>();
@override

Stream<UserInformation> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  UserInformation get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<UserInformation> set(value) async {
    if (value is UserInformation) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
