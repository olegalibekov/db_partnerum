import 'package:db_partnerum/db_partnerum.dart';
  import 'native_types/types.dart';
class PhotosMap extends DbMap<DbString, DbString> {    PhotosMap([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<PhotosMap> get future async =>
      await super.future as PhotosMap;
      
  @override
  Stream<PhotosMap> get stream => super.getStream<PhotosMap>();
  
  @override
  Stream<PhotosMap> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  PhotosMap get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<PhotosMap> set(value, [object]) async {
    if (value is PhotosMap) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
