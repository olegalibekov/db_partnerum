import 'package:db_partnerum/db_partnerum.dart';
import 'native_types/duration_type.dart';

class City extends DbBaseType{
City([String key]) : super(key);

DbString get name => get(DbString('name'));
set name(value) => name.set(value);
StationsList get stations => get(StationsList('stations', (key) => DbString(key)));
set stations(value) => stations.set(value);
@override

Future<City> get future async => await super.future as City;
@override

Stream<City> get stream => super.getStream<City>();
@override

Stream<City> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
    @override
  City get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<City> set(value) async {
    if (value is City) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    
}
///пакетная загрузка нужна еще
