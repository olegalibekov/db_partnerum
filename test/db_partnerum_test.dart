import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:db_partnerum/db_partnerum.dart' hide Message, Notification;
import 'package:googleapis/cloudtasks/v2.dart';
import 'package:googleapis/fcm/v1.dart';
import 'package:intl/intl.dart';

import '../bin/utils/custom_client.dart';
import '../bin/utils/server_auth.dart';
import 'package:http/http.dart' as http;

//TODO check several uploads.
Future<void> main() async {
  serverAuth.scopes.add(FirebaseCloudMessagingApi.cloudPlatformScope);
  serverAuth.scopes.add(CloudTasksApi.cloudPlatformScope);
  await serverAuth.init();
  //cachedStreamTest();
  // db.etagCacheOn = true;
  // var databaseDescription = root.userData['QCyzVldODYZdxZbVh7Ygj9siIZ22']
  //     .houses.future;
  //loadData();
  // testStatuses();
  // testMessages();
  // testNotifications();
  // testTasks();
  //testFileUpload();
  //testOptimizationSimplified();
  //testOptimization();
  //testUpdate();
  //testJustUpdate();
  // deleteImg();
}
/*
deleteImg() async {
  var img = await root.userData['geOUdojvEEMEXhc2ts44BzZzekV2'].houses['-M_aupjQsoTLiH4v3AzO'].houseInformation.photos['-M_auqrP9UHS2y8UeEVP']
      .bigImg.metadataFuture;
  img.remove();
}

testImageRemoval() async {
  var house = db.root.userData['test'].houses..settings.packet.on();
  var file = house.push.houseInformation.photos.push;
  var setted = file.smallImg
      .set(FileWrapper('README.txt', File('README.md').readAsBytesSync()));
  print('upload time');
  await house.upload();
  await setted;
  print(file.smallImg.value);
  print(await file.smallImg.urlWithToken);
  file.smallImg.remove();
}
testTokenGeneration() async {
  var house = db.root.userData['test'].houses..settings.packet.on();
  var file = house.push.houseInformation.photos.push;
  var setted = file.smallImg
      .set(FileWrapper('README.txt', File('README.md').readAsBytesSync()));
  print('upload time');
  await house.upload();
  await setted;
  print(file.value);
  print(await file.smallImg.urlWithToken);
}
*/
testAsyncAndOptimization() async {
  db.asyncMode = true;
  db.optimizationMode = true;
  var root = db.root;
  root.userData['test'].userInformation.username = '0';
  root.userData['test'].userInformation.username = '1';

  root.userData['test'].userInformation.username = '2';
  root.userData['test'].userInformation.rating = 0.5;
  await Future.delayed(Duration(milliseconds: 500));
  root.userData['test'].userInformation.username = '3';
  root.userData['test'].userInformation.username = '4';
  await Future.delayed(Duration(milliseconds: 500));
  root.userData['test'].userInformation.username = '5';
}

testLocalCaching() async {
  db.etagCacheOn = true;
  Map container = {};
  try {
    var tmp = jsonDecode(File('test.txt').readAsStringSync() ?? '');
    if (tmp is Map) {
      container = tmp;
    }
  } catch (_) {}
  db.getter = (String key) async {
    await Future.delayed(Duration(milliseconds: 100));
    return container[key];
  };
  db.saver = (String key, String value) async {
    await Future.delayed(Duration(milliseconds: 500));
    container[key] = value;
    File('test.txt').writeAsStringSync(jsonEncode(container));
    return true;
  };
  await db.init('test');
  var databaseDescription = root.userData['QCyzVldODYZdxZbVh7Ygj9siIZ22']
      .houses['-MWE6uvw260V4Tm1UHnr'].houseInformation;
  var timestamp = DateTime.now().millisecondsSinceEpoch;
  var description = await databaseDescription.future;
  print(description.description.value);
  print(timestamp - DateTime.now().millisecondsSinceEpoch);
  print("=======");
  timestamp = DateTime.now().millisecondsSinceEpoch;
  description = await databaseDescription.future;
  print(description.description.value);
  print(timestamp - DateTime.now().millisecondsSinceEpoch);
}

cachedStreamTest() async {
  db.streamCacheOn = true;
  var databaseDescription = root.userData['QCyzVldODYZdxZbVh7Ygj9siIZ22']
      .houses['-MWE6uvw260V4Tm1UHnr'].houseInformation;
  var description = await databaseDescription.stream.first;
  print(description.description.value);
  await Future.delayed(Duration(milliseconds: 500));
  print('======');
  var timestamp = DateTime.now().millisecondsSinceEpoch;
  databaseDescription.stream.listen((event) {
    print(timestamp - DateTime.now().millisecondsSinceEpoch);
    timestamp = DateTime.now().millisecondsSinceEpoch;
    print(description.description.value);
  });
}

loadData() async {
  var databaseDescription = db.root.userData['QCyzVldODYZdxZbVh7Ygj9siIZ22']
      .houses['-MWE6uvw260V4Tm1UHnr'].houseInformation.description;
  var description = await databaseDescription.future;
  print(description.value);
}

testStatuses() async {
  var ownerStatus = (await db.root.chatTopic.offer2Room['-MXgf3jiR3nF8EYtbJHi']
      .conversationUsers.usersStatuses.ownerUserStatus.future);
  print(ownerStatus.value);
}

testMessages() {
  db.asyncMode = true;
  var messages = db.root.chatTopic.userToUser.push.messages;
  var newMessage = messages.push;
  newMessage.uid = 'geOUdojvEEMEXhc2ts44BzZzekV2';
  newMessage.timestamp = DateTime.now().millisecondsSinceEpoch;
  newMessage.message = 'new message3';
}

testFileUpload() async {
  // var newf = await db.root.userData['test'].houses['-MW40Rto17eL1FfGHDtS']
  //     .houseInformation.photos['lg37297'].future;
  // print(newf.uint8list);
  // print(newf.value);
  // print(newf.url);
  // print(newf.syncHttpHeaders);

  // return;
  var house = db.root.userData['test'].houses..settings.packet.on();
  var file = house.push.houseInformation.photos.push;
  file.set(FileWrapper('README.txt', File('README.md').readAsBytesSync()));
  print('upload time');
  await house.upload();
  // var newFile = await db.root.userData['test'].houses['tets'].houseInformation
  //     .photos['-MVabDykKfasV5FFBg1f'].future;
  // print(file.uint8list);
  //return;
  // var rooms = await db
  //     .root.userData['test'].houses['tets'].houseInformation.rooms.future;
  // print(rooms.value);
  //
  // var photo = db.root.userData['test'].houses['tets'].houseInformation
  //     .photos['-MVabDykKfasV5FFBg1f'];
  //
  // await photo.future;
  // await photo.remove();
  // await photo.future;
  // File('test.txt').writeAsBytesSync(photo.uint8list);
  // print(photo.uint8list);

  //   var img = house.houseInformation.photos.push;
  //   var file =  FileWrapper(path.basename(image.path), image.readAsBytesSync());
  //   //print(image.readAsBytesSync());
  //   img.set(file);
  // }
  // house.upload();
}

testOptimizationSimplified() async {
  var messages = db.root.chatTopic.userToUser['hello'];
  messages.messages.settings.optimization.on();
  messages.messages['rtes'].message = 1;
  messages.messages['stes'].message = 1;
  messages.messages['otes'].message = 1;
  await Future.delayed(Duration(seconds: 5));
  messages.messages['rtes'].message = 9;

  messages.messages['stes'].message = 8;
  messages.messages['rtes'].message = 7;
  messages.messages['stes'].message = 9;

  messages.messages['rtes'].message = 17;
  messages.messages['rtes'].message = 18;
  messages.messages['rtes'].message = 19;
  messages.messages['rtes'].message = 20;
  messages.messages['rtes'].message = 21;
  messages.messages['rtes'].message = 22;
  print(messages.messages['rtes'].operationsList);
}

testOptimization() async {
  var messages = db.root.chatTopic.userToUser['hello'];
  messages.messages.settings.optimization.on();
  messages.messages['rtes'].message = 1;
  await Future.delayed(Duration(seconds: 5));
  messages.messages['rtes'].message = 2;
  messages.messages['rtes'].message = 3;
  messages.messages['rtes'].message = 4;
  messages.messages['rtes'].message = 5;
  messages.messages['rtes'].message = 6;
  messages.messages['rtes'].message = 7;
  messages.messages['rtes'].message = 8;
  messages.messages['rtes'].message = 9;

  messages.messages['stes'].message = 6;
  messages.messages['stes'].message = 7;
  messages.messages['stes'].message = 8;
  messages.messages['rtes'].message = 7;
  messages.messages['stes'].message = 9;

  messages.messages['rtes'].message = 10;
  messages.messages['rtes'].message = 11;
  messages.messages['rtes'].message = 12;
  messages.messages['rtes'].message = 13;
  messages.messages['rtes'].message = 14;
  messages.messages['rtes'].message = 15;
  messages.messages['rtes'].message = 16;
  messages.messages['rtes'].message = 17;
  messages.messages['rtes'].message = 18;
}

testJustUpdate() async {
  var messages = db.root.chatTopic.userToUser.push.messages;
  await messages.future;
  print(messages.values);
  var push = messages.push;

  await messages.push.message.set('smth');
  await push.message.set('test message');
  print(messages.values.map<String>((e) => e.message.value));
}

testUpdate() async {
  var operations = [
    {
      ['test', '1']: '1'
    },
    {
      ['test', '1']: '2'
    },
    {
      ['test', '1']: '3'
    },
    {
      ['test', '1']: '4'
    },
    {
      ['test', '1']: '5'
    },
    {
      ['test', '1']: '6'
    },
    {
      ['test', '1']: '7'
    },
    {
      ['test', '1']: '8'
    },
    {
      ['test', '1']: '9'
    },
    {
      ['test', '123', '0']: '2'
    },
    {
      ['test', '123', '1']: '3'
    },
    {
      ['test', '123', '0']: '4'
    },
    {
      ['test', '123', '1']: '5'
    },
    {
      ['test', '123', '0']: '6'
    },
    {
      ['test', '123', '1']: '7'
    },
    {
      ['test', '1']: '9'
    },
    {
      ['test', '1']: '9'
    },
    {
      ['test', '1']: '9'
    },
    {
      ['test', '1']: '9'
    },
  ];

  //for()

  //return;

  // await RealtimeDatabaseManager.update([
  //   'test',
  //   'two',
  //   'cool'
  // ], {
  //   'one': 'two',
  //   'three': 'two',
  //   'nice': {'soDeep': 'yeah'}
  // });
  //
  // await RealtimeDatabaseManager.update([
  //   'test',
  //   'two',
  //   'cool'
  // ], {
  //   'one': '2',
  // });
  // //return;
  // await RealtimeDatabaseManager.update([
  //   'test',
  //   'two',
  // ], {
  //   'cool': {
  //     'one': 'two',
  //     'three': '2',
  //   }
  // });
}

testValueRemoving() async {
  var messages = db.root.chatTopic.userToUser.push.messages;
  await messages.future;
  print(messages.values);
  var push = messages.push;

  await messages.push.set('smth');
  await push.set('test message');
  print(messages.values);

  await push.remove();
  print(messages.values);
}

testValueRemovingV2() async {
  var messages = db.root.chatTopic.userToUser.push.messages;
  await messages.future;
  print(messages.values);
  var push = messages.push;

  await messages.push.set('smth');
  await push.set('test message');
  //await messages.future;
  print(messages.values);

  print('10 секунд до проверки состояния');
  await Future.delayed(Duration(seconds: 10));
  print('время кончилось');
  await messages.future;
  print(messages.values);
}

Future<void> testStreamV1() async {
  db.root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.stream
      .map((event) => event?.values?.toList() ?? [])
      .listen((event) {
    print('1 listener: ${event.length}');
  });

  db.root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.stream
      .map((event) => event?.values?.toList() ?? [])
      .listen((event) {
    print('2 listener: ${event.length}');
  });

  await Future.delayed(Duration(seconds: 3));

  db.root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.push
      .set('test message');

  await Future.delayed(Duration(seconds: 3));

  db.root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.stream
      .map((event) => event?.values?.toList() ?? [])
      .listen((event) {
    print('3 listener: ${event.length}');
  });

  db.root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.push
      .set('test message');

  await Future.delayed(Duration(seconds: 3));

  db.root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.stream
      .map((event) => event?.values?.toList() ?? [])
      .listen((event) {
    print('4 listener: ${event.length}');
  });
}

Future<void> testStreamV2() async {
  var root = db.root;

  root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.stream
      .map((event) => event?.values?.toList() ?? [])
      .listen((event) {
    print('1 listener: ${event.length}');
  });

  root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.stream
      .map((event) => event?.values?.toList() ?? [])
      .listen((event) {
    print('2 listener: ${event.length}');
  });

  await Future.delayed(Duration(seconds: 3));

  root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.push
      .set('test message');

  root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.push
      .set('test message');

  await Future.delayed(Duration(seconds: 3));

  root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.stream
      .map((event) => event?.values?.toList() ?? [])
      .listen((event) {
    print('3 listener: ${event.length}');
  });

  root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.push
      .set('test message');

  await Future.delayed(Duration(seconds: 3));

  root.chatTopic.userToUser['-MTkbGSpwj6NofLQyUKj'].messages.stream
      .map((event) => event?.values?.toList() ?? [])
      .listen((event) {
    print('4 listener: ${event.length}');
  });
}

testNotifications() async {
  var sendMessageRequest = SendMessageRequest()
    // ..validateOnly = true
    ..message = Message();
  sendMessageRequest.message
    ..notification = Notification()
    ..token =
        "eE5oIC1YQeC4mAqaoYya-D:APA91bESsy9tO7roec6mG6LME1mBSJzDPaNRSvBRVRgAd5FnstEHmLcajwF1u1-wVVKpFKmRFNTkMMEhc4_1QR_T5XLGDd1qCWJ3zSNaDXSZH7t7qUicFoYGwKQ7W47XBKQdJrlIM65z";
  // "dQcmY1fsSmiXoDusXsR8fZ:APA91bEuVd3YlrWS54g7BE1Apl9jxunwbNDEMktaR33_pgVFT3xO5obacFe-nLLgPGRqzmVBgwdoeDRS059tfvm5yfcrG6iFYDIMdQE2kWn4y";
  sendMessageRequest.message.notification.title = 'test one';
  sendMessageRequest.message.notification.body = 'test one body';
  sendMessageRequest.message.notification.image;

  var client = CustomClient(headers: {
    'Authorization': 'Bearer ${await db.realtimeIdTokenGeneratorServer()}'
  });

  Platform.environment;

  try {
    var message = await FirebaseCloudMessagingApi(client)
        .projects
        .messages
        .send(sendMessageRequest, 'projects/${db.projectId}');
    print(message);
  } on DetailedApiRequestError catch (e) {
    if (e.status == 400) {
      print("Такого токена не существует");
    }
    print(e);
    return client.close();
  } finally {
    print("end");
  }
  client.close();
}

testTasks() async {
  //review00timeout
  var request = CreateTaskRequest();
  request.task = Task();
  request.task.httpRequest = HttpRequest();
  request.task.httpRequest
    ..headers = {"Content-type": "application/json"}
    ..url = 'https://9b5f14c6edc7.ngrok.io'
    ..httpMethod = 'POST'
    ..bodyAsBytes =
        utf8.encode(jsonEncode({'url': 'user_data/{key1}/houses/{key2}'}));
  var sheduledTime = DateTime.now().add(Duration(seconds: 10));
  sheduledTime = sheduledTime.subtract(sheduledTime.timeZoneOffset);
  var formattedTime =
      DateFormat('yyyy-MM-ddTHH:mm:ss.SSS').format(sheduledTime);
  request.task.scheduleTime = '${formattedTime}Z';
  var parent =
      'projects/${db.projectId}/locations/${'europe-west1'}/queues/${'review00timeout'}';

  request.task.name = parent + '/tasks/test_${Random().nextInt(1000)}';
  print(request.task.name);
  var client = CustomClient(headers: {
    'Authorization': 'Bearer ${await db.realtimeIdTokenGeneratorServer()}'
  });
  var que = Queue();
  que.name = 'review00timeout';

  var task = await CloudTasksApi(client)
      .projects
      .locations
      .queues
      .tasks
      .create(request, parent);

  client.close();
  print(task);
}
