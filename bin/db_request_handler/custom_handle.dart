// import 'dart:html';
/// для очистки Algolia - использовать эту функцию:  AlgoliaManager().index.clearIndex();
// AlgoliaManager().index.clearIndex();
import 'dart:io';

import 'package:async/async.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:db_partnerum/src/models/constants/status_constants.dart';
import 'package:shelf/shelf.dart';
import 'package:algolia/algolia.dart';
import 'package:path/path.dart' as path;
import '../utils/algolia_manager.dart';
import '../utils/notification_manager.dart';
import '../utils/status_solver.dart';
import 'package:micro_utils/map_path_handler.dart';


Future<Response> handle(Map map) async {
  var url = map['url'];
  switch (url) {case 'chat_topic/user_to_user/{key1}/cid':
      return newChatCreation(map);
      break;case 'images_container/{key1}':
      return deleteUselessImage(map);
      break;case 'user_data/{key1}/sum_rating':
      return changeRating(map);
      break;case 'user_data/{key1}/user_phone':
      return creatingUser(map);
      break;case 'chat_topic/user_to_user/{key1}/messages/{key2}':
      return onMessage(map);
      break;case 'user_data/{key1}/user_information/is_removed':
      return onAccountRemoved(map);
      break;case 'user_data/{key1}/houses/{key2}':
      return changeHouse(map);
      break;case 'chat_topic/offer2room/{key1}/conversation_users':
      return creatingChat(map);
      break;case 'chat_topic/user_to_user/{key1}/messages/{key2}/uid':
      return newMessage(map);
      break;case 'chat_topic/user_to_user/{key1}/messages/{key2}/timestamp':
      return newMessageTimestamp(map);
      break;case 'user_data/{key1}/requests/{key2}/request_information':
      return changeRequestInfo(map);
      break;case 'chat_topic/offer2room/{key1}/conversation_users/owner_user':
      return offerObjectCreation(map);
      break;case 'chat_topic/offer2room/{key1}/conversation_users/users_statuses':
      return statusesHandler(map);
      break;case 'chat_topic/offer2room/{key1}/review/owner_review/rating_and_review':
      return ownerRatingAndReview(map);
      break;case 'chat_topic/offer2room/{key1}/review/request_user_review/rating_and_review':
      return requestorRatingAndReview(map);
      break;case 'chat_topic/offer2room/{key1}/review/owner_review':
      return ownerReview(map);
      break;case 'chat_topic/offer2room/{key1}/review/request_user_review':
      return requestorReview(map);
      break;case 'chat_topic/offer2room/{key1}/review/request_user_review/review_answer':
      return requestorReviewAnswer(map);
      break;case 'chat_topic/offer2room/{key1}/review/owner_review/review_answer':
      return ownerReviewAnswer(map);
      break;case 'user_data/{key1}/requests/{key2}/request_information/last_change':
      return changeRequest(map);
      break;case 'chat_topic/offer2room/{key1}/conversation_users/users_statuses/request_user_status':
      return requestorStatusTracker(map);
      break;}
  return Response(404);
}
// funcs


Future<Response> changeHouse(Map jsonLog) async {
  bool checkFullInfo(Map snap) {
    return (mapPathHandler(jsonLog, ['context', 'params', 'key1']) != null) &&
        (mapPathHandler(jsonLog, ['context', 'params', 'key2']) != null) &&
        (mapPathHandler(snap, ['after', 'house_information', 'city']) != null) &&
        (mapPathHandler(snap, ['after', 'house_information', 'stations']) != null) &&
        (mapPathHandler(snap, ['after', 'house_information', 'price_range', 'start_position']) != null) &&
        (mapPathHandler(snap, ['after', 'house_information', 'price_range', 'end_position']) != null);
  }

  bool deletedHouse(Map snap) {
    return (mapPathHandler(jsonLog, ['context', 'params', 'key1']) != null) &&
        (mapPathHandler(jsonLog, ['context', 'params', 'key2']) != null) &&
        (mapPathHandler(snap, ['before']) != null) &&
        (mapPathHandler(snap, ['after']) == null);
  }

  if (jsonLog['context']['authType'] == 'USER') {
    print('changeHouse:');
    if (deletedHouse(jsonLog['snap'])) {
      var userID = jsonLog['context']['params']['key1'];
      var houseID = jsonLog['context']['params']['key2'];
      var objectID = userID + ';' + houseID;
      AlgoliaManager().index.object(objectID).deleteObject();
      // AlgoliaManager().index.clearIndex();
      print('    changeHouse deleteHouse');
    } else {
      if (checkFullInfo(jsonLog['snap'])) {
        var userID = jsonLog['context']['params']['key1'];
        var houseID = jsonLog['context']['params']['key2'];
        var objectID = userID + ';' + houseID;
        var city = jsonLog['snap']['after']['house_information']['city'];
        var stations = jsonLog['snap']['after']['house_information']['stations'].values.map((e) => e.replaceAll(' ', '_')).toList();
        var startPrice = jsonLog['snap']['after']['house_information']['price_range']['start_position'];
        var endPrice = jsonLog['snap']['after']['house_information']['price_range']['end_position'];

        var object = {'objectID': objectID, 'city': city, 'stations': stations, 'startPrice': startPrice, 'endPrice': endPrice};
        AlgoliaManager().index.object(objectID).updateData(object);
        print('    changeHouse changeHouse');
      }
    }
  }
  return Response(200);
}

Future<Response> changeRequest(Map jsonLog) async {
  print('changeRequest');
  return Response(200);

  /*
  void LoadOfferHouses({String userID, String requestID, List<AlgoliaObjectSnapshot> hits}) {
    var offerHouses = db.root.userData[userID].requests[requestID].offers;
    offerHouses.clear();
    for (var hit in hits) {
      var house = offerHouses.push;
      house.userId = hit.objectID.split(';').first;
      house.houseId = hit.objectID.split(';').last;
    }
    offerHouses.upload();
  }

  if (jsonLog['context']['authType'] != 'USER') {
    return Response(401);
  }
  return Response(200);

  var userID = jsonLog['context']['params']['key1'];
  var requestID = jsonLog['context']['params']['key2'];
  var city = jsonLog['snap']['after']['city'];
  var stations = jsonLog['snap']['after']['stations'];
  var startPrice = jsonLog['snap']['after']['price_range']['start_position'];
  var endPrice = jsonLog['snap']['after']['price_range']['end_position'];
  var price_range = [startPrice, endPrice];
  var facetList = ['city', 'stations'];
  var numericFilters = ['price_start:${price_range[0]} TO ${price_range[1]}', 'price_end:${price_range[0]} TO ${price_range[1]}'];
  var stationsFilter = '';

  if (stations != null) {
    for (var i = 0; i < stations.length; i++) {
      var station = stations[i];
      if (i != 0) stationsFilter += ' OR ';
      stationsFilter += 'stations:${station}';
    }
  }

  AlgoliaManager().index.setAnalytics(enabled: true);
  // AlgoliaManager().index.setFacetsF(value);
  AlgoliaManager().index.facetFilter(facetList);
  // AlgoliaManager().index.setNumericFilter();
  AlgoliaManager().index.setNumericAttributesForFiltering(value: numericFilters);

/*
  var options = {
    "analytics": true,

    "facets": "*,stations,city",

    "facetFilters": facetList,

    "numericFilters": [

      ['price_start:${price_range[0]} TO ${price_range[1]}',
        'price_end:${price_range[0]} TO ${price_range[1]}']
    ]
  };


  var query = algolia.instance.index('contacts').query('john');
  // Perform multiple facetFilters
  query = query.facetFilter('status:published');
  query = query.facetFilter('isDelete:false');

  // Get Result/Objects
  var snap = await query.getObjects()
*/

  var query = AlgoliaManager().index.query('');

  query = query.facetFilter('city:${city}');
  query = query.setNumericFilter(
      'startPrice:${int.parse(startPrice)} TO ${int.parse(endPrice)} AND endPrice:${int.parse(startPrice)} TO ${int.parse(endPrice)}');
  if (stationsFilter != '') {
    query = query.filters(stationsFilter);
  }
  // query = query.filters('stations:Бажовская');
  // query = query.facetFilter('stations:"Университет"');

  var snap = await query.getObjects();
  LoadOfferHouses(userID: userID, requestID: requestID, hits: snap.hits);
  return Response(200);

   */
}

Future<Response> offerObjectCreation(Map jsonLog) async {
  print('enter in offerObjectCreation');
  Future<void> copyHouseInformation(String ownerUserId, String houseId, OfferObject offerObject, String offerObjectId, String requesterId) async {
    var house = (root.userData[ownerUserId].houses[houseId]);
    var houseInformation = (await house.houseInformation.future);

    await offerObject.houseCopy.address.set(houseInformation.address.value);

    void setStations(var object, Iterable stations) {
      var stationNum = 0;
      for (DbString station in stations) {
        object.stations[stationNum.toString()] = station.key;
        stationNum += 1;
      }
    }

    offerObject.houseCopy.city = houseInformation.city.value;
    offerObject.houseCopy.description = houseInformation.description.value;
    offerObject.houseCopy.timeToMetro = houseInformation.timeToMetro.value.inMinutes;
    offerObject.houseCopy.priceRange.startPosition = houseInformation.priceRange.startPosition.value;
    offerObject.houseCopy.priceRange.endPosition = houseInformation.priceRange.endPosition.value;

    houseInformation.photos.values.forEach((element) async {
      // var smallPhotoFuture = await element.smallImg.future;
      // var bigPhotoFuture = await element.bigImg.future;
      var photoCopy = offerObject.houseCopy.photos[element.value] = element.value;
      var usages = root.imagesContainer[element.value].usagePlacesAmount;
      var res = (await usages.future).value + 1;
      usages.set(res);
      // await photoCopy.smallImg.set(FileWrapper(path.basename(smallPhotoFuture.value), smallPhotoFuture.uint8list));
      // await photoCopy.bigImg.set(FileWrapper(path.basename(bigPhotoFuture.value), bigPhotoFuture.uint8list));
    });

    offerObject.houseCopy.rooms = houseInformation.rooms.value;
    setStations(offerObject.houseCopy, houseInformation.stations.values);
    offerObject.houseCopy.isArchived = houseInformation.isArchived.value;

    house.houseRequests[offerObjectId].offerId.set(offerObjectId);
    house.houseRequests[offerObjectId].isArchived.set(false);
    house.houseRequests[offerObjectId].requesterId.set(requesterId);
  }

  Future<void> copyRequestInformation(String requesterId, String requestId, OfferObject offerObject) async {
    var requestInformation = (await root.userData[requesterId].requests[requestId].requestInformation.future);

    void setStations(RequestInformation requestInformation, Iterable stations) {
      for (DbString station in stations) {
        requestInformation.stations[station.key] = station.key;
      }
    }

    offerObject.requestCopy.city = requestInformation.city.value;
    offerObject.requestCopy.commision = requestInformation.commision.value;
    offerObject.requestCopy.datesRange.first = requestInformation.datesRange.first.value.inMilliseconds;
    offerObject.requestCopy.datesRange.last = requestInformation.datesRange.last.value.inMilliseconds;
    offerObject.requestCopy.description = requestInformation.description.value;
    offerObject.requestCopy.guests = requestInformation.guests.value;
    offerObject.requestCopy.timeToMetro = requestInformation.timeToMetro.value.inMilliseconds;
    offerObject.requestCopy.position.latitude = requestInformation.position.latitude.value;
    offerObject.requestCopy.position.longitude = requestInformation.position.longitude.value;
    offerObject.requestCopy.rooms = requestInformation.rooms.value;
    setStations(offerObject.requestCopy, requestInformation.stations.values);
    offerObject.requestCopy.isArchived = requestInformation.isArchived.value;
    offerObject.requestCopy.priceRange.startPosition = requestInformation.priceRange.startPosition.value;
    offerObject.requestCopy.priceRange.endPosition = requestInformation.priceRange.endPosition.value;
    offerObject.requestCopy.isArchived = false;
  }

  var offerObjectId = mapPathHandler(jsonLog, ['context', 'params', 'key1']);
  var ownerUserIDAndHouseId = mapPathHandler(jsonLog, ['snap', 'after']);

  if ((offerObjectId != null) && (ownerUserIDAndHouseId != null)) {
    var ownerUserId = ownerUserIDAndHouseId.split(';').first;
    var houseId = ownerUserIDAndHouseId.split(';').last;

    var offerObject = root.chatTopic.offer2Room[offerObjectId];

    var requestUser = await offerObject.conversationUsers.requestUser.future;
    var requesterId = requestUser.value;

    var requestId = (await offerObject.requestId.future).value;

    await Future.wait([
      copyHouseInformation(ownerUserId, houseId, offerObject, offerObjectId, requesterId),
      copyRequestInformation(requesterId, requestId, offerObject)
    ]);

    var requesterNameFuture = await root.userData[requesterId].userInformation.username.future;
    var requesterName = requesterNameFuture.value;

    var messageData = statusSolver(StatusConstants.houseRequestSended, '', '', requesterName, offerObject?.houseCopy?.address?.value);
    var notificationManager = NotificationManager();
    await notificationManager.sendStatusNotification([ownerUserId], messageData['title'], messageData['body'], offerObjectId);

    // print('offerObjectCreation');
  }

  return Response(200);
}

Future<Response> changeRequestInfo(Map jsonLog) async {
  Future<void> sendNotification(String userId, String requestId, String apartmentsNumber) async {
    var notificationManager = NotificationManager();

    String notificationMessage() {
      var lastApartmentsNumberChar = apartmentsNumber.substring(apartmentsNumber.length - 1);
      if (apartmentsNumber == '0') {
        return 'По Вашей заявке не найдено квартир';
      } else if (lastApartmentsNumberChar == '1') {
        return 'По Вашей заявке найдена $apartmentsNumber квартира';
      } else if (lastApartmentsNumberChar == '2' || lastApartmentsNumberChar == '3' || lastApartmentsNumberChar == '4') {
        return 'По Вашей заявке найдено $apartmentsNumber квартиры';
      } else if (lastApartmentsNumberChar == '5' ||
          lastApartmentsNumberChar == '6' ||
          lastApartmentsNumberChar == '7' ||
          lastApartmentsNumberChar == '8' ||
          lastApartmentsNumberChar == '9' ||
          lastApartmentsNumberChar == '0') {
        return 'По Вашей заявке найдено $apartmentsNumber квартир';
      }

      return 'Завершен поиск по вашей заявке';
    }

    await notificationManager.sendFinishedApartmentsSearchNotification(userId, 'Результат подбора', notificationMessage(), requestId);
  }

  Future<void> LoadOfferHouses({String userID, String requestID, List<AlgoliaObjectSnapshot> hits}) async {
    var offerHouses = db.root.userData[userID].requests[requestID].offers;
    await offerHouses.clear();
    for (var hit in hits) {
      if (hit.objectID.split(';').first != userID) {
        var house = offerHouses.push;
        house.userId.set(hit.objectID.split(';').first);
        house.houseId.set(hit.objectID.split(';').last);
      }
    }

    /// await?
    // await offerHouses.upload();

    await sendNotification(userID, requestID, '${hits.length}');
  }

  Future<void> searchInAlgolia(List facetList, List numericFilters, String city, String startPrice, String endPrice, String stationsFilter,
      String userID, String requestID) async {
    AlgoliaManager().index.setAnalytics(enabled: true);
    // AlgoliaManager().index.setFacetsF(value);
    AlgoliaManager().index.facetFilter(facetList);
    // AlgoliaManager().index.setNumericFilter();
    // AlgoliaManager().index.setNumericAttributesForFiltering(value: numericFilters);

    var query = AlgoliaManager().index.query('');

    query = query.facetFilter('city:${city}');
    query = query.setNumericFilter('startPrice:0 TO ${endPrice} AND endPrice:${startPrice} TO 99999999');
    // query = query.setNumericFilter("['startPrice:0 TO ${endPrice}', 'endPrice:${startPrice} TO 999999999999']");
    // query = query.setNumericFilter("[['startPrice:${startPrice} TO ${endPrice}', 'price_end:${startPrice} TO ${endPrice}']]");
    // query = query.setNumericFilter();
    if (stationsFilter != '') {
      query = query.filters(stationsFilter);
    }

    var snap = await query.getObjects();
    await LoadOfferHouses(userID: userID, requestID: requestID, hits: snap.hits);
  }

  if (jsonLog['context']['authType'] != 'USER') {
    return Response(401);
  }

  var timestampBefore = mapPathHandler(jsonLog, ['snap', 'before', 'last_change']);
  var timestampAfter = mapPathHandler(jsonLog, ['snap', 'after', 'last_change']);

  var userID = mapPathHandler(jsonLog, ['context', 'params', 'key1']);
  var requestID = mapPathHandler(jsonLog, ['context', 'params', 'key2']);
  var city = mapPathHandler(jsonLog, ['snap', 'after', 'city']);
  var stations = mapPathHandler(jsonLog, ['snap', 'after', 'stations'])?.values?.toList();
  var startPrice = mapPathHandler(jsonLog, ['snap', 'after', 'price_range', 'start_position']);
  var endPrice = mapPathHandler(jsonLog, ['snap', 'after', 'price_range', 'end_position']);
  var price_range = [startPrice, endPrice];
  var facetList = ['city', 'stations'];
  var numericFilters = ['price_start:${0} TO ${price_range[1]}', 'price_end:${price_range[0]} TO 999999999999999999'];
  var stationsFilter = '';

  if (stations != null) {
    for (var i = 0; i < stations.length; i++) {
      String station = stations[i];
      if (i != 0) stationsFilter += ' OR ';
      stationsFilter += 'stations:${station.replaceAll(' ', '_')}';
    }
  }

  bool checkAllDataCollected() {
    return (timestampAfter != null) &&
        (userID != null) &&
        (requestID != null) &&
        (city != null) &&
        (stations != null) &&
        (startPrice != null) &&
        (endPrice != null) &&
        (timestampAfter != timestampBefore);
  }

  if (checkAllDataCollected()) {
    await searchInAlgolia(facetList, numericFilters, city, startPrice, endPrice, stationsFilter, userID, requestID);
    print('changeRequestInfo');
  }

  return Response(200);
}

Future<Response> statusesHandler(Map jsonLog) async {
  bool enoughToStart(Map jsonLog) {
    return (jsonLog['snap']?.containsKey('after') ?? false) && (jsonLog['snap']['after']?.containsKey('request_user_status') ?? false);
  }

  var ownerStatusBefore = [''];
  var ownerStatusAfter = [''];
  if (enoughToStart(jsonLog)) {
    // print('offerId: ${(jsonLog['context']['params'])}');
    var rStatusBefore = mapPathHandler(jsonLog, ['snap', 'before', 'request_user_status']) ?? '';
    // if ((jsonLog['snap']?.containsKey('before') ?? false) && (jsonLog['snap']['before']?.containsKey('request_user_status') ?? false)) {
    //   rStatusBefore = jsonLog['snap']['before']['request_user_status'];
    // }

    ownerStatusBefore = mapPathHandler(jsonLog, ['snap', 'before', 'owner_user_status'])?.split(';') ?? [''];
    // if ((jsonLog['snap']?.containsKey('before') ?? false) && (jsonLog['snap']['before']?.containsKey('owner_user_status') ?? false)) {
    //   ownerStatusBefore = jsonLog['snap']['before']['owner_user_status']?.split(';') ?? [''];
    // }
    var oStatusBefore = ownerStatusBefore[0];

    ownerStatusAfter = mapPathHandler(jsonLog, ['snap', 'after', 'owner_user_status'])?.split(';') ?? [''];
    // if (jsonLog['snap']['after']?.containsKey('owner_user_status') ?? false) {
    //   ownerStatusAfter = jsonLog['snap']['after']['owner_user_status']?.split(';') ?? [''];
    // }
    var oStatusAfter = ownerStatusAfter[0];
    var oRequestDenyReasonAfter = '';

    if (ownerStatusAfter.length > 1) oRequestDenyReasonAfter = ownerStatusAfter[1];

    var rStatusAfter = jsonLog['snap']['after']['request_user_status'];
    var offerId = jsonLog['context']['params']['key1'];

    var address = await db.root.chatTopic.offer2Room[offerId].houseCopy.address.future;
    var ownerUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var requestUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.requestUser.future;

    var addressValue = address.value;
    var ownerId = ownerUser.value.split(';').first;
    var requestUserId = requestUser.value;

    if (addressValue == null || ownerId == null || requestUserId == null) return Response(401);

    var ownerNameFuture = await db.root.userData[ownerId].userInformation.username.future;
    var requestUserNameFuture = await db.root.userData[requestUserId].userInformation.username.future;

    var ownerName = ownerNameFuture.value ?? '';
    var requestUserName = requestUserNameFuture.value ?? '';

    var ownerStatusChange = (oStatusBefore != oStatusAfter);

    var senderUserName = ownerStatusChange ? ownerName : requestUserName;
    var recipientUserId = ownerStatusChange ? requestUserId : ownerId;

    var messageData = statusSolver(ownerStatusChange ? oStatusAfter : rStatusAfter, ownerStatusChange ? oStatusBefore : rStatusBefore,
        ownerStatusChange ? rStatusBefore : oStatusBefore, senderUserName, addressValue, oRequestDenyReasonAfter);

    var notificationManager = NotificationManager();

    var usersIdList = <String>[];
    usersIdList.add(recipientUserId);

    if (messageData != 'error')
      await notificationManager.sendStatusNotification(
          usersIdList, messageData['title'].toString(), messageData['body'].toString(), offerId.toString());

    // adding booking dates to houseInfo
    if ((oStatusAfter == 'request_booked') && (oStatusBefore != 'request_booked' )) {
      var houseId = ownerUser.value.split(';').last;
      var datesRange = await root.chatTopic.offer2Room[offerId].requestCopy.datesRange.future;
      var startDate = datesRange.first.value;
      var endDate = datesRange.last.value;
      var ownerAutoDatesAddFuture = await root.userData[ownerId]?.apartmentsAutoBookingDates?.future;
      var ownerAutoDatesAdd = ownerAutoDatesAddFuture?.value ?? true;
      if (ownerAutoDatesAdd == true) {
        var newBookingPeriod = root.userData[ownerId].houses[houseId].bookingDates.push;
        await newBookingPeriod.checkIn.set(startDate.inMilliseconds);
        newBookingPeriod.checkOut.set(endDate.inMilliseconds);
        root.chatTopic.offer2Room[offerId].conversationUsers.bookingDatesId.set(newBookingPeriod.key);
      }
    }

    if ((oStatusAfter == 'request_denied') && (oStatusBefore != 'request_denied')) {
      var bookingPeriodIdFuture = await root.chatTopic.offer2Room[offerId].conversationUsers.bookingDatesId.future;
      var bookingPeriodId = bookingPeriodIdFuture?.value;
      if (bookingPeriodId != null){
        var houseId = ownerUser.value.split(';').last;
        root.userData[ownerId].houses[houseId].bookingDates[bookingPeriodId].remove();
      }
    }

  }
  return Response(200);
}

Future<Response> requestorStatusTracker(Map jsonLog) async {
  var status = mapPathHandler(jsonLog, ['snap', 'after']);
  if (status == 'house_request_canceled') {
    var offerId = jsonLog['context']['params']['key1'];
    var ownerUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var ownerId = ownerUser.value.split(';').first;
    var houseId = ownerUser.value.split(';').last;
    var photos = await db.root.userData[ownerId].houses[houseId].houseInformation.photos.future;
    photos.values.forEach((element) async {
      var usages = root.imagesContainer[element.value].usagePlacesAmount;
      var prev = (await usages.future);
      print('prev: ${prev}');
      var res = prev.value - 1;
      usages.set(res);
    });
    print('requestorStatusTracker');
  }
  return Response(200);
}

Future<Response> creatingChat(Map jsonLog) async {
  if ((mapPathHandler(jsonLog, ['snap', 'after', 'chat_id']) != null) && (mapPathHandler(jsonLog, ['snap', 'before', 'chat_id']) == null)) {
    var chatId = jsonLog['snap']['after']['chat_id'];
    var ownerId = jsonLog['snap']['after']['owner_user'].split(';').first;
    var requesterId = jsonLog['snap']['after']['request_user'];

    db.root.chatTopic.userToUser[chatId].users['0'] = ownerId;
    db.root.chatTopic.userToUser[chatId].users['1'] = requesterId;

    var ownerInfo = await root.userData[ownerId].userInformation.future;
    root.userData[requesterId].userChats.otr[chatId].companionInfo.username.set(ownerInfo.username.value);
    root.userData[requesterId].userChats.otr[chatId].companionInfo.avatarId.set(ownerId);

    // root.chatTopic.userToUser[chatId].usersInfo[ownerId].username.set(ownerInfo.username.value);
    //TODO add avatar copy
    // root.chatTopic.userToUser[chatId].usersInfo[ownerId].avatar.set(ownerInfo.avatar.value);

    var requesterInfo = await root.userData[requesterId].userInformation.future;
    root.userData[ownerId].userChats.otr[chatId].companionInfo.username.set(requesterInfo.username.value);
    root.userData[ownerId].userChats.otr[chatId].companionInfo.avatarId.set(requesterId);

    // root.chatTopic.userToUser[chatId].usersInfo[requesterId].username.set(requesterInfo.username.value);
    //TODO add avatar copy
    // root.chatTopic.userToUser[chatId].usersInfo[requesterId].avatar.set(requesterInfo.avatar.value);

    // var ownerName = (await db.root.userData[ownerId].userInformation.username.future).value;
    // var requestorName = (await db.root.userData[requesterId].userInformation.username.future).value;
    var addOwnerChat = db.root.userData[ownerId].userChats.otr[chatId];
    addOwnerChat.status.set(chatId);
    addOwnerChat.companionId = requesterId;
    // addOwnerChat.companionName = requestorName;
    var addRequestChat = db.root.userData[requesterId].userChats.otr[chatId];
    addRequestChat.status.set(chatId);
    addRequestChat.companionId = ownerId;
    // addRequestChat.companionName = ownerName;
    print('creatingChat');
  }
  return Response(200);
}

Future<Response> ownerReview(Map jsonLog) async {
  // if (jsonLog['snap'].containsKey('before') &&
  //     jsonLog['snap']['after']['timestamp'] !=
  //         jsonLog['snap']['before']['timestamp'])

  print('ownerReview:');
  if (jsonLog['snap'].containsKey('before') &&
      jsonLog['snap'].containsKey('after') &&
      jsonLog['snap']['after']['rating_and_review'] != jsonLog['snap']['before']['rating_and_review']) {
    // var authorId = jsonLog['snap']['after']['author_id'];
    var offerId = jsonLog['context']['params']['key1'];

    var ratingAndReview = splitRatingAndReview(jsonLog['snap']['after']['rating_and_review']);
    var rating = ratingAndReview[0];
    var reviewText = ratingAndReview[1];

    var reviewAnswer = jsonLog['snap']['after']['review_answer'];
    var requestUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.requestUser.future;
    // var ownerUser = await db
    //     .root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var requestorId = requestUser.value;
    // var ownerUserId = ownerUser.value.split(';').first;

    var newReview = root.userData[requestorId].reviews[offerId];
    newReview.reviewText.set(reviewText);
    newReview.answerText = reviewAnswer;
    newReview.mark = rating + 0.01;

    var prevSum = (await db?.root?.userData[requestorId]?.sumRating?.future)?.value ?? 0.01;
    prevSum += rating;
    db.root.userData[requestorId].sumRating = prevSum;

    var prevNum = (await db?.root?.userData[requestorId]?.reviewsNumber?.future)?.value ?? 0;
    prevNum += 1;
    db.root.userData[requestorId].reviewsNumber = prevNum;
    print('    ownerReview (rating)');
  }

  if (jsonLog['snap'].containsKey('before') &&
      jsonLog['snap'].containsKey('after') &&
      jsonLog['snap']['after']['review_answer'] != jsonLog['snap']['before']['review_answer']) {
    var offerId = jsonLog['context']['params']['key1'];
    var reviewAnswer = jsonLog['snap']['after']['review_answer'];
    var requestUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.requestUser.future;
    var requesterId = requestUser.value;

    var newReview = root.userData[requesterId].reviews[offerId];
    newReview.answerText = reviewAnswer;
    print('    ownerReview (text)');
  }

  return Response(200);
}

Future<Response> requestorReview(Map jsonLog) async {
  // if (jsonLog['snap']['after']['timestamp'] !=
  //     jsonLog['snap']['before']['timestamp'])

  print('requestorReview:');

  if (jsonLog['snap'].containsKey('before') &&
      jsonLog['snap'].containsKey('after') &&
      jsonLog['snap']['after']['rating_and_review'] != jsonLog['snap']['before']['rating_and_review']) {
/*
    var offerId = jsonLog['context']['params']['key1'];
    var reviewText = jsonLog['snap']['after']['review_text'];
    var rating = jsonLog['snap']['after']['rating'];
    var reviewAnswer = jsonLog['snap']['after']['review_answer'];
*/
    var offerId = jsonLog['context']['params']['key1'];

    var ratingAndReview = splitRatingAndReview(jsonLog['snap']['after']['rating_and_review']);
    var rating = ratingAndReview[0];
    var reviewText = ratingAndReview[1];

    var reviewAnswer = jsonLog['snap']['after']['review_answer'];

    var ownerUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var ownerId = ownerUser.value.split(';').first;

    var newReview = root.userData[ownerId].reviews[offerId];
    newReview.reviewText.set(reviewText);
    newReview.answerText = reviewAnswer;
    newReview.mark = rating + 0.01;
    var prevSum = (await db?.root?.userData[ownerId]?.sumRating?.future)?.value ?? 0.01;
    prevSum += rating;
    db.root.userData[ownerId].sumRating = prevSum;

    var prevNum = (await db?.root?.userData[ownerId]?.reviewsNumber?.future)?.value ?? 0;
    prevNum += 1;
    db.root.userData[ownerId].reviewsNumber = prevNum;
    print('    requestorReview (rating)');
  }

  if (jsonLog['snap'].containsKey('before') &&
      jsonLog['snap'].containsKey('after') &&
      jsonLog['snap']['after']['review_answer'] != jsonLog['snap']['before']['review_answer']) {
    var offerId = jsonLog['context']['params']['key1'];

    var reviewAnswer = jsonLog['snap']['after']['review_answer'];

    var ownerUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var ownerId = ownerUser.value.split(';').first;

    var newReview = root.userData[ownerId].reviews[offerId];
    newReview.answerText.set(reviewAnswer);
    print('    requestorReview (text)');
  }
  return Response(200);
}

Future<Response> newMessage(Map jsonLog) async {
  var chatId = mapPathHandler(jsonLog, ['context', 'params', 'key1']);
  var messageAuthorId = mapPathHandler(jsonLog, ['snap', 'after']);
  if ((chatId != null) && (messageAuthorId != null)) {
    var users = (await db.root.chatTopic.userToUser[chatId].users.future.then((value) => value.values.toList()));
    var secondUserId = (users[0].key == messageAuthorId) ? users[1].key : users[0].key;
    print('secondUserId: ${secondUserId}, users[0].key: ${users[0].key}, messageAuthorId: ${messageAuthorId}');
    var isActiveChat = (await db?.root?.userData[secondUserId]?.activeChats[chatId]?.future)?.value == true;
    if (!isActiveChat) {
      var type = (await root.chatTopic.userToUser[chatId].header.type.future);
      if (type.value == 'offer2room') {
        root.userData[secondUserId].userChats.otr[chatId].status = 'new message';
        root.userData[secondUserId].unreadChats[chatId] = chatId;
      } else {
        root.userData[secondUserId].userChats.utu[chatId].status = 'new message';
        root.userData[secondUserId].unreadChats[chatId] = chatId;
      }
    }
    print('newMessage');
  }
  return Response(200);
}

Future<Response> changeRating(Map jsonLog) async {
  var ratedUserId = mapPathHandler(jsonLog, ['context', 'params', 'key1']);
  var sumRating = mapPathHandler(jsonLog, ['snap', 'after']);
  if ((ratedUserId != null) && (sumRating != null)) {
    var numberOfReviews = (await db.root.userData[ratedUserId].reviewsNumber.future).value;

    db.root.userData[ratedUserId].userInformation.rating = (sumRating - 0.01) / numberOfReviews;
    print('changeRating');
  }
  return Response(200);
}

Future<Response> newMessageTimestamp(Map jsonLog) async {
  var chatId = mapPathHandler(jsonLog, ['context', 'params', 'key1']);
  var newTimestamp = mapPathHandler(jsonLog, ['snap', 'after']);
  if ((chatId != null) && (newTimestamp != null)) {
    var timestampInDB = await db.root.chatTopic.userToUser[chatId].timestamp.future;
    await timestampInDB.set(newTimestamp);
    print('newMessageTimestamp');
  }
  return Response(200);
}

Future<Response> onMessage(Map jsonLog) async {
  var message = mapPathHandler(jsonLog, ['snap', 'after', 'message']);
  var timestamp = mapPathHandler(jsonLog, ['snap', 'after', 'timestamp']);
  var senderUserId = mapPathHandler(jsonLog, ['snap', 'after', 'uid']);
  if ((message != null) && (senderUserId != null) && (timestamp != null)) {
    var chatId = jsonLog['context']['params']['key1'];

    var chatUsers = await db.root.chatTopic.userToUser[chatId].users.future;
    List<String> recipientsId = [];
    for (var element in chatUsers.values) {
      var userId = element.key;
      if (userId != senderUserId) {
        var userOpenedChat = await db.root?.userData[userId]?.activeChats[chatId]?.future;
        if (userOpenedChat?.value != null) break;
        recipientsId.add(userId);
      }
    }

    if (recipientsId.isNotEmpty) {
      var senderUserName = await db.root.userData[senderUserId].userInformation.username.future;
      var notificationManager = NotificationManager();
      await notificationManager.sendMessageNotification(chatId, recipientsId, 'Сообщение от ${senderUserName?.value ?? 'пользователя'}', message);
    }
    print('onMessage');
  }

  return Response(200);
}

Future<Response> newChatCreation(Map jsonLog) async {
  var chatId = mapPathHandler(jsonLog, ['snap', 'after']);
  if (chatId != null) {
    var firstUserId = (await db.root.chatTopic.userToUser[chatId].users['0'].future).value;
    var secondUserId = (await db.root.chatTopic.userToUser[chatId].users['1'].future).value;

    db.root.userData[firstUserId].availableUsers[secondUserId].chatId = chatId;
    db.root.userData[secondUserId].availableUsers[firstUserId].chatId = chatId;

    // var firstUserName = (await db.root.userData[firstUserId].userInformation.username.future).value;
    // var secondUserName = (await db.root.userData[secondUserId].userInformation.username.future).value;

    var firstUserChat = db.root.userData[firstUserId].userChats.utu[chatId];
    firstUserChat.status.set(chatId);
    firstUserChat.companionId = secondUserId;
    // firstUserChat.companionName = secondUserName;

    var secondUserChat = db.root.userData[secondUserId].userChats.utu[chatId];
    secondUserChat.status.set(chatId);
    secondUserChat.companionId = firstUserId;
    // secondUserChat.companionName = firstUserName;
    print('newChatCreation');
  }
  return Response(200);
}

Future<Response> onAccountRemoved(Map jsonLog) async {
  var userId = jsonLog['context']['params']['key1'];

  declineRequestUserOffers() async {
    var requests = await db.root?.userData[userId]?.requests?.future;
    if (requests?.values != null) {
      requests.values.forEach((element) async {
        var offers = await element?.offers?.future;
        if (offers?.values != null) {
          offers.values.forEach((element) async {
            var offerId = element?.offerId?.value;
            if (offerId != null) {
              await db.root.chatTopic.offer2Room[offerId].conversationUsers.usersStatuses.requestUserStatus.set('house_request_canceled');
            }
          });
        }
      });
    }
  }

  declineOwnerUserOffers() async {
    var houses = await db.root?.userData[userId]?.houses?.future;
    if (houses?.values != null) {
      houses.values.forEach((element) async {
        var houseRequestsList = await element?.houseRequests?.future;
        if (houseRequestsList.values != null) {
          houseRequestsList.values.forEach((element) async {
            var offerId = element?.value;
            if (offerId != null) {
              await db.root.chatTopic.offer2Room[offerId].conversationUsers.usersStatuses.ownerUserStatus.set('request_denied');
            }
          });
        }
      });
    }
  }

  var isRemoved = mapPathHandler(jsonLog, ['snap', 'after']);

  if (isRemoved == true) {
    declineRequestUserOffers();
    declineOwnerUserOffers();

    var notificationManager = NotificationManager();
    await notificationManager.sendAccountRemoveNotification([userId], 'Удаление аккаунта', 'Вход в учетную запись недоступен');
    print('onAccountRemoved');
  }

  return Response(200);
}

Response deleteUselessImage(Map jsonLog) {
  var bigImg = mapPathHandler(jsonLog, ['snap', 'after', 'big_img']);
  var photosAmount = mapPathHandler(jsonLog, ['snap', 'after', 'usage_places_amount']);
  if (bigImg != '0') {
    if (photosAmount == 0) {
      var pid = jsonLog['context']['params']['key1'];
      root.imagesContainer[pid].smallImg.remove();
      root.imagesContainer[pid].bigImg.remove();
      root.imagesContainer[pid].remove();
      print('deleteUselessImage');
    } else {}
  }
  return Response(200);
}

Response creatingUser(Map jsonLog) {
  var uid = mapPathHandler(jsonLog, ['context', 'params', 'key1']);
  if (uid != null) {
    var support = db.root.userData[uid].availableUsers.push;
    support.userInformation.username = 'support';
    support.userInformation.uid = 'support';
    support.userInformation.role = 'support';
    print('creatingUser');
  }
  return Response(200);
}

Future<Response> ownerRatingAndReview(Map jsonLog) async {
  var offerId = mapPathHandler(jsonLog, ['context', 'params', 'key1']);
  var ratingAndReview = mapPathHandler(jsonLog, ['snap', 'after']);

  if ((offerId != null) && (ratingAndReview != null)) {
    var requestUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.requestUser.future;
    var ownerUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var chat = await db.root.chatTopic.offer2Room[offerId].conversationUsers.chatId.future;

    var requestorId = requestUser.value;
    var ownerUserId = ownerUser.value.split(';').first;
    var chatId = chat.value;

    ratingAndReviewNotification(ownerUserId, requestorId, ratingAndReview, chatId);
    print('ownerRatingAndReview');
  }
  return Response(200);
}

Future<Response> ownerReviewAnswer(Map jsonLog) async {
  var reviewAnswer = mapPathHandler(jsonLog, ['snap', 'after']);
  var offerId = mapPathHandler(jsonLog, ['context', 'params', 'key1']);

  if ((reviewAnswer != null) && (offerId != null)) {
    var requestUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.requestUser.future;
    var ownerUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var chat = await db.root.chatTopic.offer2Room[offerId].conversationUsers.chatId.future;

    var requestorId = requestUser.value;
    var ownerUserId = ownerUser.value.split(';').first;
    var chatId = chat.value;

    reviewAnswerNotification(offerId, requestorId, ownerUserId, reviewAnswer, chatId);
    print('ownerReviewAnswer');
  }
  return Response(200);
}

Future<Response> requestorRatingAndReview(Map jsonLog) async {
  var offerId = mapPathHandler(jsonLog, ['context', 'params', 'key1']);
  var ratingAndReview = mapPathHandler(jsonLog, ['snap', 'after']);

  if ((offerId != null) && (ratingAndReview != null)) {
    var requestUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.requestUser.future;
    var ownerUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var chat = await db.root.chatTopic.offer2Room[offerId].conversationUsers.chatId.future;

    var requestorId = requestUser.value;
    var ownerUserId = ownerUser.value.split(';').first;
    var chatId = chat.value;

    ratingAndReviewNotification(requestorId, ownerUserId, ratingAndReview, chatId);
    print('requestorRatingAndReview');
  }
  return Response(200);
}

Future<Response> requestorReviewAnswer(Map jsonLog) async {
  var reviewAnswer = mapPathHandler(jsonLog, ['snap', 'after']);
  var offerId = mapPathHandler(jsonLog, ['context', 'params', 'key1']);

  if ((offerId != null) && (reviewAnswer != null)) {
    var requestUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.requestUser.future;
    var ownerUser = await db.root.chatTopic.offer2Room[offerId].conversationUsers.ownerUser.future;
    var chat = await db.root.chatTopic.offer2Room[offerId].conversationUsers.chatId.future;

    var requestorId = requestUser.value;
    var ownerUserId = ownerUser.value.split(';').first;
    var chatId = chat.value;

    reviewAnswerNotification(offerId, ownerUserId, requestorId, reviewAnswer, chatId);
    print('requestorReviewAnswer');
  }
  return Response(200);
}

ratingAndReviewNotification(String senderId, String recipientId, String ratingAndReview, String chatId) async {
  var ratingAndReviewSplitted = splitRatingAndReview(ratingAndReview);
  var rating = ratingAndReviewSplitted[0];
  var review = ratingAndReviewSplitted[1];

  var senderUserNameFuture = await db.root.userData[senderId].userInformation.username.future;
  var senderUserName = userName(senderUserNameFuture.value);
  var notificationManager = NotificationManager();

  String message;
  if (rating == null && review == null) {
    message = 'Отзыв о Вас был удален';
  } else if (review.isNotEmpty) {
    message = '${senderUserName} оставил Вам отзыв "$review" и рейтинг "$rating"';
  } else if (review.isEmpty) {
    message = '${senderUserName} оставил Вам рейтинг "$rating"';
  }
  await notificationManager.sendReviewNotification(recipientId, 'Отзыв', message, chatId);
}

reviewAnswerNotification(String offerId, String senderId, String recipientId, String reviewAnswer, String chatId) async {
  if (reviewAnswer.isNotEmpty) {
    var ownerUserFuture = await db.root.userData[senderId].userInformation.username.future;
    var ownerUserName = userName(ownerUserFuture.value);

    var notificationManager = NotificationManager();

    var message = '${ownerUserName} прокомментировал Ваш отзыв: "$reviewAnswer"';
    await notificationManager.sendReviewNotification(recipientId, 'Ответ на Ваш отзыв', message, chatId);
  }
}

List<dynamic> splitRatingAndReview(String value) {
  if (value == null || value.isEmpty) return [null, null];
  var idx = value.indexOf(';');
  var parts = <dynamic>[value.substring(0, idx).trim(), value.substring(idx + 1).trim()];
  parts[0] = double.parse(parts[0]);
  return parts;
}
