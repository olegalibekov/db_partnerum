import 'dart:io';

import 'package:db_partnerum/db_partnerum.dart' as dbPartnerum;
import 'package:googleapis/cloudtasks/v2.dart';
import 'package:googleapis/fcm/v1.dart';

import 'custom_client.dart';

class NotificationManager {
  sendMessageNotification(
      String chatId, List<String> usersId, String title, String body) async {
    usersId.forEach((userId) async {
      await sendForEachUserToken(
          'message', userId, title, body, null, null, null, chatId, null);
    });
  }

  sendStatusNotification(List<String> usersId, String title, String body,
      String offerObjectId) async {

    var houseImagesFuture = await dbPartnerum
        .root?.chatTopic?.offer2Room[offerObjectId]?.houseCopy?.photos?.future;

    String firstHouseImageUrl;

    try {
      var firstHouseImageKey = await houseImagesFuture?.values?.first?.value;
      var firstHouseImageFuture = await dbPartnerum
          .root?.imagesContainer[firstHouseImageKey]?.bigImg?.future;
      firstHouseImageUrl = await firstHouseImageFuture?.urlWithToken;
    } catch (_) {
      firstHouseImageUrl =
      'https://licenciasmurcia.com/wp-content/uploads/2020/12/Licencias-de-actividad-en-Murcia-contactar-1024x871.png';
    }

    usersId.forEach((userId) async {
      var notificationId = await sendNotificationToDB(
          userId, title, body, firstHouseImageUrl, offerObjectId, null);
      await sendForEachUserToken('status', userId, title, body,
          firstHouseImageUrl, notificationId, offerObjectId, null, null);
    });
  }

  sendAccountRemoveNotification(
    List<String> usersId,
    String title,
    String body,
  ) async {
    usersId.forEach((userId) async {
      await sendForEachUserToken(
          'accountRemove', userId, title, body, null, null, null, null, null);
    });
  }

  sendFinishedApartmentsSearchNotification(
      String userId, String title, String body, String requestId) async {
    var imageLink =
        'https://d8vlg9z1oftyc.cloudfront.net/multicoisashomolog/multicoisashomolog-file-manager/IMAGES/ilustracaaao-cidade.png';
    await sendForEachUserToken('finishedApartmentsSearch', userId, title, body,
        imageLink, null, null, null, requestId);
  }

  sendReviewNotification(
      String userId, String title, String body, String chatId) async {
    // var imageUrl =
    //     "https://www.digitalsquad.in/wp-content/uploads/2021/01/online-PR-management-services-in-kolkata-1024x685.png";

    var notificationId =
        await sendNotificationToDB(userId, title, body, null, null, chatId);
    await sendForEachUserToken('review', userId, title, body, null,
        notificationId, null, chatId, null);
  }

  sendNotificationToDB(String userId, String title, String body, String image,
      String offerObjectId, String chatId) async {
    var notification = dbPartnerum.db.root.userData[userId].notifications.push;

    // await because of getting push id
    await notification.read.set(false);

    notification.timestamp.set(DateTime.now().millisecondsSinceEpoch);
    if (offerObjectId != null) {
      notification.offerObjectId.set(offerObjectId);
    } else if (chatId != null) {
      notification.chatId.set(chatId);
    }
    notification.notification.body.set(body);
    notification.notification.image.set(image);
    notification.notification.title.set(title);

    return notification.key;
  }

  sendForEachUserToken(
      String notificationType,
      String userId,
      String title,
      String body,
      String image,
      String notificationId,
      String offerObjectId,
      String chatId,
      String requestId) async {
    var userTokens = (await dbPartnerum
            .db.root.userData[userId].userInformation.tokens.future)
        .values;
    if (userTokens != null && userTokens.isNotEmpty) {
      userTokens.forEach((userToken) async {
        var userTokenKey = userToken.key.toString();
        var sendMessageRequest = await createSendMessageRequest(
            notificationType,
            userTokenKey,
            title,
            body,
            image,
            notificationId,
            offerObjectId,
            chatId,
            requestId);
        await notificationResponse(userId, userTokenKey, sendMessageRequest);
      });
    }
  }

  createSendMessageRequest(
      String notificationType,
      String userToken,
      String title,
      String body,
      String image,
      String notificationId,
      String offerObjectId,
      String chatId,
      String requestId) {
    var sendMessageRequest = SendMessageRequest();
    sendMessageRequest.message = Message();
    var message = sendMessageRequest.message;
    message..notification = Notification();

    message.token = userToken;
    message.notification
      ..title = title
      ..body = body;

    if (image != null) message.notification..image = image;

    message.data = {'notificationType': notificationType};

    if (notificationId != null) message.data['notificationId'] = notificationId;
    if (offerObjectId != null) message.data['offerObjectId'] = offerObjectId;
    if (chatId != null) message.data['chatId'] = chatId;
    if (requestId != null) message.data['requestId'] = requestId;

    return sendMessageRequest;
  }

  void notificationResponse(String userId, String token,
      SendMessageRequest sendMessageRequest) async {
    ///ToDo: Check status
    ///ToDo: Check client.close();

    Platform.environment;

    var client = CustomClient(headers: {
      'Authorization':
          'Bearer ${await dbPartnerum.db.realtimeIdTokenGeneratorServer()}'
    });

    try {
      var message = await FirebaseCloudMessagingApi(client)
          .projects
          .messages
          .send(sendMessageRequest, 'projects/${dbPartnerum.db.projectId}');
      print(message);
    } on DetailedApiRequestError catch (e) {
      // await dbPartnerum.db.root.userData[userId].userInformation.tokens[token]
      //     .remove(token);
      print(e);
      return client.close();
    }
    client.close();
  }
}

// void main() async {
//   serverAuth.scopes.add(FirebaseCloudMessagingApi.cloudPlatformScope);
//   serverAuth.scopes.add(CloudTasksApi.cloudPlatformScope);
//   await serverAuth.init();
//
//   // var messageData = statusSolver(
//   //     StatusConstants.requestBooked,
//   //     StatusConstants.houseRequestSended,
//   //     StatusConstants.requestBooked,
//   //     'OppositeUserId',
//   //     'houseAddress');
//
//   var notificationManager = NotificationManager();
//
//   // await notificationManager.sendNotification(
//   //     ["geOUdojvEEMEXhc2ts44BzZzekV2"],
//   //     messageData['title'],
//   //     messageData['body'],
//   //     'https://assets.ifttt.com/images/channels/651849913/icons/large.png',
//   //     'OfferObjectId');
//   // await notificationManager.sendStatusNotification(
//   //     ["QCyzVldODYZdxZbVh7Ygj9siIZ22"],
//   //     'Заголовок',
//   //     'Текст сообщения',
//   //     'https://www.pngfind.com/pngs/m/212-2129306_change-icon-png-shake-hand-circle-icon-transparent.png',
//   //     'OfferObjectId');
// }
