import 'dart:convert';
import 'dart:io';

import 'package:db_partnerum/db_partnerum.dart';
import 'package:googleapis_auth/auth_io.dart';


var gToken;

class ServerAuth {
  var scopes = [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/firebase.database',
    'https://www.googleapis.com/auth/devstorage.full_control',
    'https://www.googleapis.com/auth/devstorage.read_only',
    'https://www.googleapis.com/auth/devstorage.read_write'
  ];

  static final ServerAuth _singleton = ServerAuth._internal();
  static const String gServiceAccountPath = 'assets/g-service-account.json';
  static AutoRefreshingAuthClient client;
  ServiceAccountCredentials serviceAccountCredentials;

  Future<String> tokenViaServiceAccount() async {
    if (gToken != null) return gToken;
    client = await clientViaServiceAccount(serviceAccountCredentials, scopes);
    var token = client.credentials.accessToken.data;
    gToken = token;
    client.close();
    return token;
  }

  init() async {
    serviceAccountCredentials = ServiceAccountCredentials.fromJson(
        jsonDecode(File(gServiceAccountPath).readAsStringSync()));
    db.isWeb = false;
    db.realtimeIdTokenGeneratorServer = tokenViaServiceAccount;
    await tokenViaServiceAccount();
  }

  factory ServerAuth() {
    return _singleton;
  }

  ServerAuth._internal();
}

final serverAuth = ServerAuth();

