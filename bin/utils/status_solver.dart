import 'package:db_partnerum/src/models/constants/status_constants.dart';

stringHasData(String value) {
  if (value == null || value.isEmpty) return false;
  return true;
}

userName(String name) {
  return !stringHasData(name)
      ? "Анонимный пользователь"
      : "Пользователь ${name}";
}

statusSolver(String newStatus, String statusBefore, String oppositeUserStatus,
    String senderUserName, String houseAddress,
    [String reason]) {
  if (newStatus == StatusConstants.houseRequestSended &&
      statusBefore == StatusConstants.houseRequestCanceled) {
    // Повторно подал заявку
    var messageData = {};
    messageData['title'] = "Повторно подал заявку";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " повторно подал заявку на вашу квартиру ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.houseRequestCanceled &&
      oppositeUserStatus == StatusConstants.requestBooked &&
      statusBefore == StatusConstants.requestBooked) {
    // Отменил бронь и заявку
    var messageData = {};
    messageData['title'] = "Отменили бронь и заявку";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " отменил бронь и заявку вашей квартиры ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.houseRequestSended &&
      oppositeUserStatus == StatusConstants.requestBooked &&
      statusBefore == StatusConstants.requestBooked) {
    // Отменил бронь
    var messageData = {};
    messageData['title'] = "Отменили бронь";
    messageData['body'] = userName(senderUserName);
    messageData['body'] += " отменил бронь квартиры ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.requestBooked &&
      oppositeUserStatus == StatusConstants.requestBooked &&
      statusBefore != StatusConstants.requestBooked) {
    // Возобновление брони
    var messageData = {};
    messageData['title'] = "Бронь возобновили";
    messageData['body'] = userName(senderUserName);
    messageData['body'] += " возобновил бронь квартиры ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.requestAccept &&
      oppositeUserStatus == StatusConstants.requestBooked &&
      statusBefore == StatusConstants.requestBooked) {
    // Отменил предложение брони
    var messageData = {};
    messageData['title'] = "Отменил предложение брони";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " отменил предложение брони квартиры ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.requestDenied &&
      oppositeUserStatus == StatusConstants.requestBooked &&
      statusBefore == StatusConstants.requestBooked) {
    //Отменили бронь и отказались от сотрудничества
    var messageData = {};
    messageData['title'] = "Полностью отказались от сотрудничества";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " отменил бронь, и отказался сотрудничать, квартиры ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.houseRequestCanceled &&
      (oppositeUserStatus == StatusConstants.requestAccept ||
          oppositeUserStatus.isEmpty)) {
    // Отменил заявку
    var messageData = {};
    messageData['title'] = "Отменили заявку";
    messageData['body'] = userName(senderUserName);
    messageData['body'] += " отменил заявку на вашу квартиру ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.houseRequestCanceled &&
      oppositeUserStatus == StatusConstants.requestBooked) {
    // Отменил предложение брони и заявку
    var messageData = {};
    messageData['title'] = "Отозвали заявку и бронь";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " отозвал заявку и бронь на вашу квартиру ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.houseRequestSended &&
      oppositeUserStatus.isEmpty) {
    // Подал заявку
    var messageData = {};
    messageData['title'] = "Подали заявку";
    messageData['body'] = userName(senderUserName);
    messageData['body'] += " подал заявку на вашу квартиру ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.houseRequestSended &&
      oppositeUserStatus == StatusConstants.requestAccept) {
    // Повторно подал заявку
    var messageData = {};
    messageData['title'] = "Повторно подал заявку";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " повторно подал заявку на вашу квартиру ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.houseRequestSended &&
      oppositeUserStatus == StatusConstants.requestBooked) {
    // Отменил предложение брони
    var messageData = {};
    messageData['title'] = "Отменили предложение брони";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " отменил предложение брони вашей квартиры ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.requestBooked &&
    oppositeUserStatus == StatusConstants.requestAccept) {
    // Предложение о брони
    var messageData = {};
    messageData['title'] = "Вам предложили бронь";
    messageData['body'] = userName(senderUserName);
    messageData['body'] += " хочет взять в бронь вашу квартиру ${houseAddress}";
    return messageData;
  }
  if (newStatus == StatusConstants.requestAccept &&
      oppositeUserStatus != StatusConstants.houseRequestCanceled) {
    // Заявка на квартиру одобрена. Можно приступить к обсуждению деталей
    var messageData = {};
    messageData['title'] = "Заявка одобрена";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " одобрил вашу заявку на квартиру ${houseAddress}. Можно приступать к обсуждению деталей.";
    return messageData;
  }
  if (newStatus == StatusConstants.requestAccept &&
      (statusBefore == StatusConstants.houseRequestCanceled)) {
    // Заявка на квартиру вновь одобрена. Можно повторно приступить к обсуждению деталей
    var messageData = {};
    messageData['title'] = "Заявка вновь одобрена";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " вновь одобрил вашу заявку на квартиру ${houseAddress}. Можно приступать к обсуждению деталей.";
    return messageData;
  }
  if (newStatus == StatusConstants.requestDenied &&
      oppositeUserStatus == StatusConstants.houseRequestSended) {
    // Заявку отклонили
    var messageData = {};
    messageData['title'] = "Заявку отклонили";
    messageData['body'] = userName(senderUserName);
    messageData['body'] += " отклонил вашу заявку на квартиру ${houseAddress}.";
    if (stringHasData(reason)) messageData['body'] += " По причине: ${reason}";
    return messageData;
  }
  if (newStatus == StatusConstants.requestDenied &&
      oppositeUserStatus == StatusConstants.requestBooked) {
    //Отменили предложение брони и отказались от сотрудничества
    var messageData = {};
    messageData['title'] =
        "Отменили предложение брони и отказались от сотрудничества";
    messageData['body'] = userName(senderUserName);
    messageData['body'] += " отклонил вашу заявку на квартиру ${houseAddress}.";
    if (stringHasData(reason)) messageData['body'] += " По причине: ${reason}";
    return messageData;
  }
  if (newStatus == StatusConstants.requestBooked &&
      oppositeUserStatus == StatusConstants.requestBooked) {
    //Бронь подтверждена
    var messageData = {};
    messageData['title'] = "Бронь согласована";
    messageData['body'] = userName(senderUserName);
    messageData['body'] +=
        " согласовал предложение брони квартиры ${houseAddress}";
    return messageData;
  }

  /// ToDo: rate status
  // if (value == rate) {
  //   //Оценили работу с Вами и Вашей квартирой.
  //   messageData['title'] = "Работу с Вами оценили";
  //   messageData['body'] = userName(anotherUser['username']);
  //   messageData['body'] += " оценил работу с Вами. Комментарий: ${data.comment}";
  //   data.image = "";
  // }
  // if (value == rate_answer) {
  //   messageData['title'] = "На Ваш комментарий ответили";
  //   messageData['body'] = userName(anotherUser['username']);
  //   messageData['body'] += " ответил Вам в комментарии: ${data.comment}";
  //   data.image = "";
  // }

  return "error";
  // if (newStatus == StatusConstants.houseRequestCanceled &&
  //     oppositeUserStatus == StatusConstants.requestDenied) {
  //   // Не может быть такого. Ничего не присылаем.
  // }
  // if (newStatus == StatusConstants.requestBooked && !oppositeUserStatus) {
  //   // *
  // }
  // if (newStatus == StatusConstants.requestBooked &&
  //     oppositeUserStatus == StatusConstants.requestDenied) {
  //   // *
  // }
  // if (newStatus == StatusConstants.requestAccept &&
  //     (oppositeUserStatus == StatusConstants.houseRequestCanceled ||
  //         !oppositeUserStatus)) {
  //   //*
  // }
  // if (newStatus == StatusConstants.requestDenied &&
  //     (oppositeUserStatus == StatusConstants.houseRequestCanceled ||
  //         !oppositeUserStatus)) {
  //   // *
  // }
  // if (newStatus == StatusConstants.requestBooked &&
  //     (oppositeUserStatus == StatusConstants.houseRequestSended ||
  //         oppositeUserStatus == StatusConstants.houseRequestCanceled)) {
  //   //*
  // }
  // if (newStatus == StatusConstants.houseRequestSended &&
  //     oppositeUserStatus == StatusConstants.requestDenied) {
  //   // *Ничего не присылаем..
  // }

  /// простите, запрос некорректный.
}
