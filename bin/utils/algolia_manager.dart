import 'dart:io';

import 'package:algolia/algolia.dart';

class AlgoliaManager {
  static const String ALGOLIA_APP_ID = 'ALGOLIA_APP_ID';
  static const String ALGOLIA_API_KEY = 'ALGOLIA_API_KEY';
  static const String ALGOLIA_INDEX_NAME = 'ALGOLIA_INDEX_NAME';

  static final AlgoliaManager _singleton = AlgoliaManager._internal();
  Algolia algolia;
  AlgoliaIndexReference index;

  factory AlgoliaManager() {
    return _singleton;
  }

  AlgoliaManager._internal() {
    algolia = Algolia.init(
      applicationId: Platform.environment[ALGOLIA_APP_ID],
      apiKey: Platform.environment[ALGOLIA_API_KEY],
    );

    index = algolia.instance.index(Platform.environment[ALGOLIA_INDEX_NAME]);

    index.setHitsPerPage(1000);
    index.setAttributesForFaceting(['city', 'stations']);
    index.setAttributesToHighlight(['none']);
    index.setAttributesToRetrieve(['objectID']);

  }
}
