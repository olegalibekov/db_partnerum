import 'dart:async';
import 'dart:io';

import 'package:db_partnerum/db_partnerum.dart';
import 'package:googleapis/cloudtasks/v2.dart';
import 'package:googleapis/fcm/v1.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart';

import 'db_request_handler/handler_center.dart';
import 'utils/server_auth.dart';

Future main() async {
  serverAuth.scopes.add(FirebaseCloudMessagingApi.cloudPlatformScope);
  serverAuth.scopes.add(CloudTasksApi.cloudPlatformScope);
  db.asyncMode = true;
  db.optimizationMode = true;
  await serverAuth.init();

  var port = int.tryParse(Platform.environment['PORT'] ?? '1235');

  var server = await serve(
    Pipeline().addMiddleware(logRequests()).addHandler(handler),
    InternetAddress.anyIPv4,
    port,
  );
}

